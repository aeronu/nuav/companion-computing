#!/usr/bin/env python
import os
import textwrap
from argparse import ArgumentParser, RawDescriptionHelpFormatter


if __name__=="__main__":
    parser = ArgumentParser(prog='protocol_splitter_agent',
      formatter_class=RawDescriptionHelpFormatter,
      epilog=textwrap.dedent('''\
         Example:
             ros2 run companion_computing protocol_splitter_agent.py -i 1 -n drone_0/RTPS
         '''))
    parser.add_argument("-n", "--namespace", default=None, type=str, help="ROS namespace")
    parser.add_argument("-s", "--serial-device", default="/dev/ttyACM0", type=str, help="UART device")
    parser.add_argument("-b", "--baud", default=1000000, type=int, help="Baud rate")
    args, _ = parser.parse_known_args()
    rtps_name = f"{args.namespace}/RTPS"
    
    os.system(f"protocol_splitter -d {args.serial_device} -b {args.baud} & micrortps_agent -t UDP -n {rtps_name} -r 5901 -s 5900")
