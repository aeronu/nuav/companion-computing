#!/usr/bin/env python3
'''
start_sitls.launch.py

Starts specified number of PX4 SITLs. Useful if you want SITLs to fully load before simulation.
'''


import launch_ros
from launch import LaunchDescription
from launch.actions import OpaqueFunction, DeclareLaunchArgument
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from companion_computing.launch.common import get_local_arguments, get_launch_arguments

import os


DEFAULT_BUILD_PATH = f"{os.environ['PX4_AUTOPILOT']}/build/px4_sitl_rtps"
LAUNCH_ARGS = [
    {"name": "nb",              "default": "1",                         "description": "Number of vehicles to spawn.",
        "type": "int"},
    {"name": "models",           "default": "['iris']",             "description": "List of models. Will autofill if number spawned is greater than number of items. Ex: ['iris','octo_x'].",
        "type": "list"},
    {"name": "px4_console",     "default": "false",                     "description": "Flag for starting interactive PX4 console.",
        "type": "bool"},
    {"name": "build_path",      "default": DEFAULT_BUILD_PATH,          "description": "Build path for PX4."}
]


def launch_setup(context, *args, **kwargs):
    """Allows declaration of launch arguments within the ROS2 context
    """
    args = get_local_arguments(LAUNCH_ARGS, context)
    models = process_models(args["models"], args["nb"])

    ld = []
    # Spawn Vehicles
    for i, model in enumerate(models):
        log_directory = f"{args['build_path']}/instance_{i}"
        # Launch PX4 SITL with specified instance and model type
        ld.append(
            launch_ros.actions.Node(
                package='px4_utils', executable='px4_sitl.py',
                output='screen',
                arguments=[
                    "--instance", str(i),
                    "--log-directory", log_directory,
                    "--build-path", args["build_path"],
                    "--model", model,
                    f'{"--px4-console" if args["px4_console"] else ""}'
                ]
            )
        )
    return ld


def generate_launch_description():   
    launch_description = []
    launch_description += get_launch_arguments(LAUNCH_ARGS)
    launch_description += [OpaqueFunction(function = launch_setup)] 
    return LaunchDescription(launch_description)


def process_models(models, nb):
    """Pre-process models."""
    new_models = models.copy()
    len_ns = len(new_models)
    if nb > len_ns and models == 1:
        gen_num = nb - len_ns  # amount to generate
        for i in range(gen_num):
            new_models.append(models[0])
    elif nb > len_ns:
        raise ValueError("nb launch arg cannot be used if models launch arg is not size 1")
    return new_models
