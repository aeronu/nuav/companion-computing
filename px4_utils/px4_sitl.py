#!/usr/bin/env python
import os
import textwrap
from argparse import ArgumentParser, RawDescriptionHelpFormatter


if __name__=="__main__":
    '''
    To use PX4 alone run these commands in a terminal: 
    ```bash
    export PX4_SIM_MODEL=iris
    $PX4_AUTOPILOT/build/px4_sitl_rtps/bin/px4 -i 0 $PX4_AUTOPILOT/build/px4_sitl_rtps/etc -w $PX4_AUTOPILOT/build/px4_sitl_rtps/instance_0/sitl_iris_0 -s $PX4_AUTOPILOT/build/px4_sitl_rtps/etc/init.d-posix/rcS
    ```
    '''
    parser = ArgumentParser(prog='px4_sitl',
    formatter_class=RawDescriptionHelpFormatter,
    epilog=textwrap.dedent('''\
        Starts PX4 SITL.
        '''))
    parser.add_argument("-i", "--instance", default=0, type=int, help="Instance of vehicle.")
    parser.add_argument("-m", "--model", default="iris", type=str, help="Vehicle model to start.")
    parser.add_argument("-d", "--log-directory", type=str, help="Directory to store log files.")
    parser.add_argument("-b", "--build-path", type=str, help="Path to build folder of PX4-Autopilot.")
    parser.add_argument("-c", "--px4-console", action='store_true', default=False, help="Enables the px4 console.")
    args, _ = parser.parse_known_args()

    # Setups variables
    i = args.instance
    model = args.model
    log_directory = args.log_directory
    build_path = args.build_path

    # Create folder for storing vehicle data
    os.system(f'[ ! -d "{log_directory}" ] && mkdir -p "{log_directory}"')

    # Set PX4 environment variables
    os.environ["PX4_SIM_MODEL"] = model
    os.environ["PX4_ESTIMATOR"] = "ekf2"

    # Launch PX4 SITL
    if args.px4_console:
        os.system(f'gnome-terminal -- {build_path}/bin/px4 -i {i} "{build_path}/etc" -w {log_directory}/sitl_{model}_{i} -s {build_path}/etc/init.d-posix/rcS >{log_directory}/sitl_out.log 2>{log_directory}/sitl_err.log')
    else:
        os.system(f'{build_path}/bin/px4 -i {i} -d "{build_path}/etc" -w {log_directory}/sitl_{model}_{i} -s {build_path}/etc/init.d-posix/rcS >{log_directory}/sitl_out.log 2>{log_directory}/sitl_err.log')
