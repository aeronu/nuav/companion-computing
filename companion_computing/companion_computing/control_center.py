#!/usr/bin/env python3
'''
Control Center
=======================

Contains class for controlling multiple vehicles from one commander. Uses various algorithms to
distribute high level paths to vehicles.
'''
from argparse import ArgumentParser
import rclpy
from rclpy.callback_groups import ReentrantCallbackGroup
from rclpy.executors import MultiThreadedExecutor
from rclpy.logging import get_logging_severity_from_string
from rclpy.node import Node
from companion_computing_interfaces.msg import Coordinate

import numpy as np
from companion_computing.cli.drone_client import DroneClient
from companion_computing.cli.common import complete_clients
import time


# TODO: how should the control center be handled across all drones? should each drone run it locally
#  and swap control dynmaically? this would require some weird logic making sure all control centers
#  are synced. 
class ControlCenter(Node):
    def __init__(self, log_level='info'):
        super().__init__("control_center") # start node
        if log_level is not None:
            self.get_logger().set_level(get_logging_severity_from_string(log_level))
        # self._default_callback_group = ReentrantCallbackGroup()  # ROS processes need to be run in parallel for this use case

        namespaces = set()
        # need to sleep because the node hasn't discovered all topics at this point
        # https://github.com/orise-robotics/diffbot2_robot/pull/7
        time.sleep(1.5)
        topic_list = self.get_topic_names_and_types()
        for topic in topic_list:
            split_topic = topic[0].split("/")
            if len(split_topic) > 2:
                namespaces.add(split_topic[1])
        if not namespaces:
            raise Exception("Could not find any valid topics")

        ###################
        ## Parameters
        ###################
        self.declare_parameter('vehicle_names', list(namespaces))
        self.declare_parameter('leader', "drone_0")  # TODO: figure out better way to designate leader

    def distribute_regions(self):
        """Distributes paths for specific regions to all other vehicles."""
        # Get all vehicles on network
        vehicle_names = self.vehicle_names
        self.get_logger().info(f"Distributing paths to these vehicles: {vehicle_names}")
        num_vehicles = len(vehicle_names)
        # Designate region for search
        # TODO: implement this
        
        # Generate paths using region
        # TODO: implement this        

        # Send paths to each drone asynchronously
        start_points = [[0.0,0] for i in range(num_vehicles)]
        end_points = [[5.0,0] for i in range(num_vehicles)]
        start_points = np.array(start_points)
        end_points = np.array(end_points)
        all_points = np.linspace(start_points, end_points, 5)
        all_points = np.concatenate((all_points, np.flipud(all_points)), 0)
        executor = MultiThreadedExecutor()
        clients = {}
        for i, name in enumerate(vehicle_names):
            clients[name] = DroneClient(namespace=name)
            executor.add_node(clients[name])
        # Send takeoffs
        takeoff_futures = []
        for name, client in clients.items():
            future = client.send_arm_takeoff(2.0)
            takeoff_futures.append(future)
        action_names = ["arm_takeoff"] * len(vehicle_names)
        if not complete_clients(executor, clients, takeoff_futures, action_names):
            self.get_logger().error("Could not tell all drones to takeoff, exiting")
            return
        # Send paths
        follow_futures = []
        for name, client in clients.items():
            points = all_points[:,i]
            points = np.apply_along_axis(convert_np_to_coordinate, 1, points)
            future = client.send_follow_coordinates(points.tolist(), 0.5)
            follow_futures.append(future)
        action_names = ["follow_coordinates"] * len(vehicle_names)
        if not complete_clients(executor, clients, follow_futures, action_names):
            self.get_logger().error("Could not get all drones to follow coordinates, exiting")
            return
        # Send lands
        land_futures = []
        for name, client in clients.items():
            points = all_points[:,i]
            points = np.apply_along_axis(convert_np_to_coordinate, 1, points)
            future = client.send_land()
            land_futures.append(future)
        action_names = ["land"] * len(vehicle_names)
        if not complete_clients(executor, clients, land_futures, action_names):
            self.get_logger().error("Could not get all drones to land, exiting")
            return

    @property
    def vehicle_names(self):
        vehicle_names = self.get_parameter("vehicle_names").value
        if not vehicle_names:
            raise Exception("`vehicle_names` parameter was not set")
        return vehicle_names


def convert_np_to_coordinate(pair: np.ndarray):
    """Converty 2d array to a Coordinate message.

    Args:
        pair (np.ndarray): Pair of x and y values.

    Returns:
        Coordinate: Coordinate message object
    """
    coord = Coordinate()
    coord.position.x = pair[0]
    coord.position.y = pair[1]
    coord.position.z = -1.5  # TODO: use z arg
    coord.heading = np.nan
    coord.frame = Coordinate.LOCAL_NED
    return coord


################
## Main
################
def main(args=None):
    rclpy.init(args=args)
    
    # Setup argument parsing
    parser = ArgumentParser()
    parser.add_argument("--log-level", default='info', choices=["info", "debug", "warn", "error", "fatal"], help='ros log level')
    vargs, _ = parser.parse_known_args()

    control_center = ControlCenter(log_level=vargs.log_level)
    
    control_center.distribute_regions()


if __name__ == '__main__':
    main()

