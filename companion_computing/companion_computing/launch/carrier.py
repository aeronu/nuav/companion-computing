#!/usr/bin/env python3
'''
Field
=====

Starts all systems related to controlling vehicle during a field test.
'''
from launch_ros.actions import Node

from ament_index_python.packages import get_package_share_directory
import os
from companion_computing.launch.common import get_local_arguments
from launch.substitutions import LaunchConfiguration


# Get relative package directories
companion_computing = get_package_share_directory('companion_computing')


DEFAULT_CC_DRONE_CONFIG = os.path.join(companion_computing, "config", "field.yml")
LAUNCH_ARGS = [
    {"name": "base_name",       "default": "carrier", 
        "description": "Prefix for all vehicles."},
    {"name": "log_level",       "default": "debug",
        "description": "Sets log level of ros nodes."},
    {"name": "serial_device",   "default": "/dev/ttyTHS1",
        "description": "Path to PX4 serial device port."},
    {"name": "cc_drone_config", "default": DEFAULT_CC_DRONE_CONFIG,     "description": "companion_computing drone config file for ROS parameters"},
]


def launch_setup(context, *args, **kwargs):
    """Allows declaration of launch arguments within the ROS2 context
    """
    args = get_local_arguments(LAUNCH_ARGS, context)
    # log_level = LaunchConfiguration('log_level').perform(context)
    # base_name = LaunchConfiguration('base_name').perform(context)
    # serial_device = LaunchConfiguration('serial_device').perform(context)

    # TODO: Figure out how we want to number drones in the field
    namespace = f"{args['base_name']}_0"
    instance = 0

    ld = []

    # Launch RTPS agent to create topics for interfacing with PX4 through ROS
    ld.append(
        Node(
            name='micrortps_agent',
            namespace=namespace,
            package='px4_utils', executable='micrortps_agent.py',
            output='screen',
            arguments=[
                "--namespace", namespace,
                "--serial-device", args["serial_device"],
                "--flight-controller"
            ]
        )
    )

    # Launch basic_drone file that starts all topics, services, and actions to control drone through ROS
    ld.append(
        Node(
            package='companion_computing', executable='basic_drone',
            output='screen',
            namespace=namespace,
            arguments=[
                "--log-level", args["log_level"],
                "--instance", str(instance)
            ],
            parameters = [{LaunchConfiguration("cc_drone_config")}],
        ),
    )

    # FIXME: temporary solution while robot_state_publisher is completed for ROS2
    # Launch TF Broadcaster
    x = y = 0.0
    z = 0.15
    ld.append(
        Node(
            package='companion_computing', executable='tf_broadcaster',
            output='screen',
            arguments=[
                "--namespace", namespace,
                "-x", str(x),
                "-y", str(y),
                "-z", str(z),
            ],
        ),
    )
    
    # Launch I2C Controller
    ld.append(
        Node(
            package='companion_computing', executable='i2c_controller',
            output='screen',
            namespace=namespace,
            arguments=[
                "--log_level", args['log_level'],
            ],
        ),
    )
    
    return ld