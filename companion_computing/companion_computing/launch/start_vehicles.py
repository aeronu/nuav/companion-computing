#!/usr/bin/env python3
'''
Start Vehicles
==============

Starts single vehicle or multi-vehicle simulation with required ROS2 nodes.
'''
from launch.substitutions.launch_configuration import LaunchConfiguration
from launch_ros.actions import Node, SetParameter
# from launch_ros.parameter_descriptions import ParameterFile
from launch.actions import IncludeLaunchDescription, LogInfo
from launch.launch_description_sources import PythonLaunchDescriptionSource

from ament_index_python.packages import get_package_share_directory
import os
import jinja2
import xacro
from enum import IntEnum
import yaml
import numpy as np

from companion_computing.launch.common import get_local_arguments
from companion_computing.launch.gazebo_world import LAUNCH_ARGS as GZ_LAUNCH_ARGS


# Get relative package directories
nuav_gazebo = get_package_share_directory('nuav_gazebo')
companion_computing = get_package_share_directory('companion_computing')
aruco_ros = get_package_share_directory('aruco_ros')
px4_utils = get_package_share_directory('px4_utils')


class VehicleType(IntEnum):
    """Type of vehicle to spawn."""
    DRONE = 0
    ROVER = 1
    PLANE = 2
    CARRIER = 3
    SWARM_CARRIER_DUO = 4
    SWARM_CARRIER_TRIO = 5
    DECACOPTER = 6


class SimType(IntEnum):
    """Type of simulation to start."""
    NONE = 0
    GAZEBO = 1

class ScenarioType(IntEnum):
    """Type of scenario to start."""
    DEFAULT = 0
    DROP = 1
    DARKNET = 2
    ARUCO = 3


DEFAULT_ARUCO_CONFIG = os.path.join(aruco_ros, "config", "sim.yml")
DEFAULT_CC_DRONE_CONFIG = os.path.join(companion_computing, "config", "sim.yml")
# Arguments with relevant info, type defaults to string
LAUNCH_ARGS = [
    {"name": "nb",              "default": "1",                         "description": "Number of vehicles to spawn.",
        "type": "int"},
    {"name": "namespaces",      "default": "[]",                        "description": "List of namespaces. Will autofill if number spawned is greater than number of items. Ex: ['drone','rover'].",
        "type": "list"},
    {"name": "base_name",       "default": "",                          "description": "Prefix for all vehicles."},
    {"name": "log_level",       "default": "debug",                     "description": "Sets log level of ros nodes."},
    {"name": "sim",             "default": "gazebo",                    "description": "Simulation to use.",
        "choices": [e.name.lower() for e in SimType]},
    {"name": "vehicle",    "default": "drone",                     "description": "Type of vehicle to spawn.",
        "choices": [e.name.lower() for e in VehicleType]},
    {"name": "hitl",            "default": "false",                     "description": "Flag to enable HITL.",
        "type": "bool"},
    {"name": "aruco_config",    "default": DEFAULT_ARUCO_CONFIG,        "description": "Path to aruco config"},
    {"name": "sitl",            "default": "true",                      "description": "Flag for starting SITL.",
        "type": "bool"},
    {"name": "px4_console",     "default": "false",                     "description": "Flag for opening PX4 console",
        "type": "bool"},
    {"name": "serial_device",   "default": "/dev/ttyACM0",              "description": "PX4 serial device port."},
    {"name": "include_camera",  "default": "true",                      "description": "Includes camera on frogs in simulation.",
        "type": "bool"},
    {"name": "cc_drone_config", "default": DEFAULT_CC_DRONE_CONFIG,     "description": "companion_computing drone config file for ROS parameters"},
    {"name": "sim_speed",       "default": "1.0",                       "description": "simulation speed factor, difficult at low speeds",
        "type": "float"},
    {"name": "default_model",   "default": "false",                     "description": "Flag for using PX4 default models instead of custom.",
        "type": "bool"},
    {"name": "scenario",             "default": "default",                    "description": "Which scenario to load.",
        "choices": [e.name.lower() for e in ScenarioType]},
    {"name": "x",               "default": "0.0",                        "description": "x position of vehicle", "type": "float"},
    {"name": "y",               "default": "0.0",                        "description": "y position of vehicle", "type": "float"},
    {"name": "z",               "default": "0.15",                       "description": "z position of vehicle", "type": "float"},
]
LAUNCH_ARGS += GZ_LAUNCH_ARGS


def launch_setup(context, *args, **kwargs):
    """Allows declaration of launch arguments within the ROS2 context
    """
    args = get_local_arguments(LAUNCH_ARGS, context)

    scenario = ScenarioType[args["scenario"].upper()]
    sim = SimType[args['sim'].upper()]
    vehicle_types, namespaces, models, vehicle_exes = extract_filled_list_args(args)

    # Establish log level
    log_level = args["log_level"]

    ld = []

    # Sets use_sim_time for all nodes after it in launch description
    ld.append(SetParameter(name='use_sim_time', value=True))

    camera_tilt = 0 if scenario == ScenarioType.DARKNET else np.pi/2
    # Start simulator
    if sim == SimType.GAZEBO:
        os.environ["PX4_SIM_SPEED_FACTOR"] = str(args["sim_speed"])
        launch_args = {arg["name"]: args[arg["name"]] for arg in GZ_LAUNCH_ARGS}

        if scenario == ScenarioType.DROP:
            launch_args["world"] = "franklin_park_drop.world"
        elif scenario == ScenarioType.DARKNET:
            launch_args["world"] = "franklin_park_darknet.world"
        elif scenario == ScenarioType.ARUCO:
            launch_args["world"] = "franklin_park_aruco.world"
        else:
            launch_args["world"] = args["world"]
        ld.append(
            IncludeLaunchDescription(
                PythonLaunchDescriptionSource(
                    [companion_computing, os.path.sep, 'launch',
                        os.path.sep, 'gazebo_world.launch.py']
                ),
                launch_arguments=launch_args.items(),
            )
        )
        # Spawn Vehicles
        for i, namespace in enumerate(namespaces):
            vehicle = vehicle_types[i]
            # TODO: Figure out how to use multiple vehicles with HITL?
            # TODO: drop scenario only supports 1 vehicle spawning
            (x, y, z) = (-10,-10,22.15) if scenario == ScenarioType.DROP else (args["x"], args["y"], args["z"])
            # NOTE: if the position is changed it must be updated in tf_broadcaster.py
            x += i*2.25
            ld += spawn_gz_vehicle(namespace=namespace, mavlink_tcp_port=4560+i,
                                   mavlink_udp_port=14560+i, hil_mode=args["hitl"], 
                                   vehicle_type=vehicle, include_camera=args["include_camera"],
                                   camera_tilt=camera_tilt, default_model=args["default_model"], x=x, y=y, z=z)

    build_path=f"{os.environ['PX4_AUTOPILOT']}/build/px4_sitl_rtps"
    if not args["hitl"] and args["sitl"]:
        ld.append(
            IncludeLaunchDescription(
                PythonLaunchDescriptionSource(
                    [os.path.join(px4_utils, 'launch', 'start_sitls.launch.py')]
                ),
                launch_arguments={
                    'nb': str(len(namespaces)),
                    'build_path': build_path,
                    'px4_console': LaunchConfiguration('px4_console'),
                    'models': str(models)
                }.items()
            )
        )
    # Spawn Vehicles
    for i, namespace in enumerate(namespaces):
        log_directory = f"{build_path}/instance_{i}"
        vehicle_exe = vehicle_exes[i]
        (x, y, z) = (-10,-10,22.15) if scenario == ScenarioType.DROP else (args["x"], args["y"], args["z"])
        # NOTE: if the position is changed it must be updated in tf_broadcaster.py
        x += i*2.25
        
        extra = [
            Node(
                name='micrortps_agent',
                namespace=namespace,
                package='px4_utils', executable='micrortps_agent.py',
                output='screen',
                arguments=[
                    "--instance", str(i),
                    "--namespace", namespace,
                    "--log-directory", log_directory,
                    "--serial-device", args["serial_device"],
                    f'{"--flight-controller" if args["hitl"] else ""}'
                ],
            ),
            Node(
                package='companion_computing', executable=vehicle_exe,
                output='screen',
                namespace=namespace,
                arguments=[
                    "--log-level", log_level,
                    "--instance", str(i)
                ],
                parameters = [{LaunchConfiguration("cc_drone_config")}],
            ),
            Node(
                package='companion_computing', executable='tf_broadcaster',
                output='screen',
                arguments=[
                    "--namespace", namespace,
                    "--camera-tilt", str(camera_tilt),
                    "-x", str(x),
                    "-y", str(y),
                    "-z", str(z),
                ],
            ),
            IncludeLaunchDescription(
                PythonLaunchDescriptionSource(
                    [aruco_ros, os.path.sep, 'launch',
                        os.path.sep, 'detect_aruco.py']
                ),
                launch_arguments={
                    'namespace': namespace,
                    'config_filepath': args['aruco_config']
                }.items()
            )
        ]
        ld += extra
    return ld


def spawn_gz_vehicle(namespace="drone_0", mavlink_tcp_port=4560, mavlink_udp_port=14560,
                     hil_mode=False, serial_device="/dev/ttyACM0", vehicle_type=VehicleType.DRONE,
                     include_camera=False, camera_tilt=0.0, default_model=False, x=0.0, y=0.0, z=0.15):
    """Spawns vehicle in running gazebo world.
    
    Args:
        namespace (str, optional): ROS namespace for all nodes. Defaults to "drone_0".
        mavlink_tcp_port (int, optional): TCP port to use with mavlink. Defaults to 4560.
        mavlink_udp_port (int, optional): UDP port to use with mavlink. Defaults to 14560.
        hil_mode (bool, optional): Flag that turns on HITL mode. Defaults to False.
        serial_device (str, optional): Path to PX4 serial device port. Defaults to "/dev/ttyACM0".
        include_camera (bool, optional): Includes camera on frogs in simulation. Disable for faster
            simulations.
        x (float, optional): Vehicle position in x meters. Defaults to 0.0.
        y (float, optional): Vehicle position in y meters. Defaults to 0.0.
        z (float, optional): Vehicle position in z meters. Defaults to 0.15.
    """
    # TODO: URDF still needs to be implemented
    # Creates tmp URDF file with frog v2
    mappings = {"namespace": namespace,
                "mavlink_tcp_port": mavlink_tcp_port,
                "mavlink_udp_port": mavlink_udp_port,
                "serial_enabled": "1" if hil_mode else "0",
                "serial_device": serial_device,
                "serial_baudrate": "921600",
                "hil_mode": "1" if hil_mode else "0",
                "include_camera": include_camera,
                "camera_tilt": camera_tilt}

    if vehicle_type == VehicleType.DRONE:
        if default_model:
            file_path=f"{os.environ['PX4_AUTOPILOT']}/Tools/sitl_gazebo/models/iris/iris.sdf.jinja"
        else:
            file_path = f'{nuav_gazebo}/models/robots/frog_v2/model.sdf.jinja'
    elif vehicle_type == VehicleType.ROVER:
        if default_model:
            raise NotImplementedError(f"Default model for {vehicle_type.name} not set")
        else:
            file_path = f'{nuav_gazebo}/models/robots/bullfrog/model.sdf.jinja'
    elif vehicle_type == VehicleType.PLANE:
        if default_model:
            raise NotImplementedError(f"Default model for {vehicle_type.name} not set")
        else:
            file_path = f'{nuav_gazebo}/models/robots/wallace/model.sdf.jinja'
    elif vehicle_type == VehicleType.CARRIER:
        if default_model:
            raise NotImplementedError(f"Default model for {vehicle_type.name} not set")
        else:
            file_path = f'{nuav_gazebo}/models/robots/swarm_carrier_v6/model.sdf.jinja'
        z = 0.44
    elif vehicle_type == VehicleType.DECACOPTER:
        if default_model:
            raise NotImplementedError(f"Default model for {vehicle_type.name} not set")
        else:
            file_path = f'{nuav_gazebo}/models/robots/swarm_carrier_v2/model.sdf.jinja'
        z = 0.44
    else:
        raise Exception("Vehicle type needs to be specified")

    tmp_path, robot_desc = parse_model(file_path, namespace, mappings)

    ld = []
    # Spawns vehicle model using SDF or URDF file
    ld.append(
        Node(
            package="gazebo_ros", executable="spawn_entity.py",
            arguments=[
                "-entity", namespace, "-file", tmp_path,
                "-robot_namespace", namespace,
                "-spawn_service_timeout", "120.0",
                "-x", str(x), "-y", str(y), "-z", str(z),
                # "-x", str(-10), "-y", str(-10), "-z", str(22.15),
                # "-R", str(0.0), "-P", str(0.0), "-Y", str(0.0)
            ],
            output="screen",
        )
    )
    # TODO: Uncomment this once a pipeline is added to convert our generated sdf to a urdf, this 
    #  may also require prepending the namespace to all joints and links in our sdf
    # if robot_desc is None:
    #     urdf_path = f'{nuav_gazebo}/models/frog_v2/urdf/frog_v2.urdf.xacro'
    #     _, robot_desc = parse_model(urdf_path, namespace, mappings)
    # # Launch robot_state_publisher to publish robot's TF frames for all joints
    # ld.append(
    #     Node(
    #         package='robot_state_publisher', executable='robot_state_publisher',
    #         output='screen',
    #         namespace=namespace,
    #         parameters=[{
    #             'robot_description': robot_desc,
    #             'ignore_timestamp': True
    #         },
    #         ]
    #     )
    # )
    return ld


def parse_model(file_path, namespace, mappings):
    robot_desc = None
    if file_path.split('.')[-1] == "xacro":
        tmp_path = f'/tmp/{namespace}.urdf'
        doc = xacro.process_file(file_path, mappings=mappings)
        robot_desc = doc.toprettyxml(indent='  ')
        tmp_file = open(tmp_path, 'w')
        tmp_file.write(robot_desc)
        tmp_file.close()
    elif file_path.split('.')[-1] == "erb":
        tmp_path = f'/tmp/{namespace}.sdf'
        cmd = "erb"
        for (key, val) in mappings.items():
            cmd += f" {key}={val}"
        cmd += f" {file_path} > {tmp_path}"
        os.system(cmd)
    elif file_path.split('.')[-1] == "jinja":
        tmp_path = f'/tmp/{namespace}.sdf'
        templateFilePath = jinja2.FileSystemLoader(os.path.dirname(file_path))
        jinja_env = jinja2.Environment(loader=templateFilePath)
        j_template = jinja_env.get_template(os.path.basename(file_path))
        output = j_template.render(mappings)
        with open(tmp_path, 'w') as sdf_file:
            sdf_file.write(output)
    else:
        tmp_path = file_path
    return tmp_path, robot_desc


def process_namespaces(namespaces, nb, base_name, vehicle_type):
    """Pre-process namespaces."""
    # Choose base name according to vehicle type
    if base_name == "":
        base_name = vehicle_type
    new_namespaces = namespaces.copy()
    len_ns = len(new_namespaces)
    if nb > len_ns:
        gen_num = nb - len_ns  # amount to generate
        for i in range(gen_num):
            new_namespaces.append(f"{base_name}_{len_ns+i}")
    if len(new_namespaces) != len(set(new_namespaces)):
        raise ValueError("Namespaces list contains duplicates")
    return new_namespaces


def process_nb_list(ls, nb):
    """Pre-process list and fill."""
    new_ls = ls.copy()
    len_ns = len(new_ls)
    if nb > len_ns and len_ns == 1:
        gen_num = nb - len_ns  # amount to generate
        for i in range(gen_num):
            new_ls.append(ls[0])
    elif nb > len_ns:
        raise ValueError(f"nb launch arg cannot be used if list is not size 1 {ls}, {nb}")
    return new_ls


def extract_filled_list_args(args):
    """Fills in namespaces, models, and vehicle_types launch arguments if one list contains
        more than one item. Returns all those lauch arguments and vehicle_exes.
    """
    # Get actual enums for types
    vehicle_type = VehicleType[args['vehicle'].upper()]
    namespaces = args["namespaces"]
    if vehicle_type == VehicleType.DRONE:
        models = ["iris"]
        vehicle_exes = ["basic_drone"]
    elif vehicle_type == VehicleType.ROVER:
        models = ["r1_rover"]
        vehicle_exes = ["basic_rover"]
        raise Exception("Rover not supported yet")
    elif vehicle_type == VehicleType.PLANE:
        models = ["plane"]
        vehicle_exes = ["basic_plane"]
        raise Exception("Plane not supported yet")
    elif vehicle_type == VehicleType.CARRIER:  # TODO: add carrier if statement
        models = ["octo_x"]  # UHL V6
        vehicle_exes = ["basic_drone"]
    elif vehicle_type == VehicleType.DECACOPTER:
        models = ["deca_planar"]  # UHL V2
        vehicle_exes = ["basic_drone"]
    elif vehicle_type == VehicleType.SWARM_CARRIER_DUO:
        models = ["iris", "octo_x"]
        vehicle_exes = ["basic_drone"]
        vehicle_type = [VehicleType.DRONE, VehicleType.CARRIER]
        namespaces = ["drone_0", "carrier_0"] if len(namespaces) <= 1 else namespaces
    elif vehicle_type == VehicleType.SWARM_CARRIER_TRIO:
        models = ["iris", "octo_x", "iris"]
        vehicle_exes = ["basic_drone"]
        vehicle_type = [VehicleType.DRONE, VehicleType.CARRIER, VehicleType.DRONE]
        namespaces = ["drone_0", "carrier_0", "drone_1"] if len(namespaces) <= 1 else namespaces
    else:
        raise Exception("Vehicle type needs to be specified")
    vehicle_types = vehicle_type if isinstance(vehicle_type, list) else [vehicle_type]
    # Determine argument with maximum size
    nb = max([len(namespaces), len(vehicle_types), len(models), len(vehicle_exes), args["nb"]])
    # Fill in args if one of the arguments has a size greater than 1 and the rest are only size 1
    models = process_nb_list(models, nb)
    vehicle_exes = process_nb_list(vehicle_exes, nb)
    vehicle_types = process_nb_list(vehicle_types, nb)
    # Pre-process namespaces
    namespaces = process_namespaces(namespaces, nb, args["base_name"], args["vehicle"])
    return vehicle_types, namespaces, models, vehicle_exes
