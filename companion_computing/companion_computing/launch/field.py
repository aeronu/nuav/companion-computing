#!/usr/bin/env python3
'''
Field
=====

Starts all systems related to controlling vehicle during a field test.
'''
from launch_ros.actions import Node
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource

from ament_index_python.packages import get_package_share_directory
import os
from companion_computing.launch.common import get_local_arguments
from launch.substitutions import LaunchConfiguration


# Get relative package directories
companion_computing = get_package_share_directory('companion_computing')
aruco_ros = get_package_share_directory('aruco_ros')
realsense_ros = get_package_share_directory('realsense2_camera')


DEFAULT_CC_DRONE_CONFIG = os.path.join(companion_computing, "config", "field.yml")
LAUNCH_ARGS = [
    {"name": "base_name",       "default": "drone", 
        "description": "Prefix for all vehicles."},
    {"name": "log_level",       "default": "debug",
        "description": "Sets log level of ros nodes."},
    {"name": "serial_device",   "default": "/dev/ttyTHS1",
        "description": "Path to PX4 serial device port."},
    {"name": "cc_drone_config", "default": DEFAULT_CC_DRONE_CONFIG,     "description": "companion_computing drone config file for ROS parameters"},
    {"name": "namespace", "default": "", "description": "namespace for vehicle"},
]


def launch_setup(context, *args, **kwargs):
    """Allows declaration of launch arguments within the ROS2 context
    """
    args = get_local_arguments(LAUNCH_ARGS, context)
    # log_level = LaunchConfiguration('log_level').perform(context)
    # base_name = LaunchConfiguration('base_name').perform(context)
    # serial_device = LaunchConfiguration('serial_device').perform(context)

    # TODO: Figure out how we want to number drones in the field
    namespace = args["namespace"]
    if namespace == "":
        namespace = f"{args['base_name']}_0"
    instance = 0

    ld = []

    # Launch RTPS agent to create topics for interfacing with PX4 through ROS
    ld.append(
        Node(
            name='micrortps_agent',
            namespace=namespace,
            package='px4_utils', executable='micrortps_agent.py',
            output='screen',
            arguments=[
                "--namespace", namespace,
                "--serial-device", args["serial_device"],
                "--flight-controller"
            ]
        )
    )

    # Launch basic_drone file that starts all topics, services, and actions to control drone through ROS
    ld.append(
        Node(
            package='companion_computing', executable='basic_drone',
            output='screen',
            namespace=namespace,
            arguments=[
                "--log-level", args["log_level"],
                "--instance", str(instance)
            ],
            parameters = [{LaunchConfiguration("cc_drone_config")}],
        ),
    )

    # FIXME: temporary solution while robot_state_publisher is completed for ROS2
    # Launch TF Broadcaster
    x = y = 0.0
    z = 0.15
    ld.append(
        Node(
            package='companion_computing', executable='tf_broadcaster',
            output='screen',
            arguments=[
                "--namespace", namespace,
                "-x", str(x),
                "-y", str(y),
                "-z", str(z),
            ],
        ),
    )
    
    # Launch GPIO Listener
    ld.append(
        Node(
            package='companion_computing', executable='gpio_listener',
            output='screen',
            namespace=namespace,
            arguments=[
                "--log_level", args['log_level'],
            ],
        ),
    )
    
    # Launch aruco detector node
    ld.append(
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                [aruco_ros, os.path.sep, 'launch',
                    os.path.sep, 'detect_aruco.py']
            ),
            launch_arguments={
                'namespace': namespace,
                'config_filepath': os.path.join(aruco_ros, 'config', 'field.yml')
            }.items()
        )
    )

    ld.append(
        Node(
            package='realsense2_camera',
            namespace=f'{namespace}/realsense',
            name="realsense",
            executable='realsense2_camera_node',
            remappings=[
                ("infra1/camera_info", "ir/camera_info"),
                ("infra1/image_rect_raw", "ir/image_raw"),
                ("infra1/image_rect_raw/compressed",
                 "ir/image_raw/compressed"),
                ("infra1/image_rect_raw/theora", "ir/image_raw/theora"),
                ("infra1/image_rect_raw/compressedDepth",
                 "ir/image_raw/compressedDepth")
            ],
            parameters=[
                {
                    'infra_width': 1280,
                    'infra_height': 720,
                    'infra_fps': 30.0,
                    'filters': "colorizer",
                    'enable_infra0': False,
                    'enable_infra2': False,
                    'enable_color': False,
                    'enable_depth': False,
                    'stereo_module.emitter_enabled': 2,
                }
            ],
            output='screen',
            emulate_tty=True,
        )
    )
    return ld
