#!/usr/bin/env python3
'''
Gazebo World
============

Launches gazebo world.
'''
from launch.actions import IncludeLaunchDescription, LogInfo
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration

from ament_index_python.packages import get_package_share_directory
import os


gazebo_ros = get_package_share_directory("gazebo_ros")
nuav_gazebo = get_package_share_directory("nuav_gazebo")

# Arguments with relevant info, type defaults to string
LAUNCH_ARGS = [
    {"name": "gui",             "default": "true",              "description": "Starts gazebo gui"},
    {"name": "server",          "default": "true",              "description": "Starts gazebo server to run simulations in background"},
    {"name": "verbose",         "default": "false",             "description": "Starts gazebo server with verbose outputs"},
    {"name": "world",           "default": "franklin_park.world",       "description": "Gazebo world to load"},
]


def launch_setup(context, *args, **kwargs):
    """Allows declaration of launch arguments within the ROS2 context
    """
    world = LaunchConfiguration("world").perform(context)

    ld = []
    ld.append(
        LogInfo(msg=[
            'Launching ', LaunchConfiguration('world')
        ]),
    )
    # If file is not absolute assume it is a world from nuav_gazebo
    ld.append(
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                [gazebo_ros, os.path.sep, 'launch', os.path.sep, 'gzserver.launch.py']),
            condition=IfCondition(LaunchConfiguration('server')),
            launch_arguments={
                'world': world,
                'verbose': LaunchConfiguration('verbose'),
            }.items(),
        )
    )
    ld.append(
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                [gazebo_ros, os.path.sep, 'launch', os.path.sep, 'gzclient.launch.py']),
            condition=IfCondition(LaunchConfiguration('gui'))
        )
    )
    return ld
