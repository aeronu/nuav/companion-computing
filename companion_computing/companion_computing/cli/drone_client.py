#!/usr/bin/env python3
'''
Drone Client
=======================

Generic drone client to ROS2 actions and topics in companion_computing. Easy publishing, calling,
and sending goals.
'''
from rclpy.action import ActionClient

from companion_computing_interfaces.action import ArmTakeoff, Land, PrecisionLand, DropMode
from companion_computing_interfaces.srv import ServoCommand
from std_srvs.srv import Trigger

import functools
from companion_computing.cli.vehicle_client import VehicleClient
from rclpy.task import Future


class DroneClient(VehicleClient):
    def __init__(self, executor, namespace=None):
        super().__init__(executor, namespace=namespace)
        self._cli_arm_takeoff = ActionClient(self, ArmTakeoff, "arm_takeoff")
        self._cli_land = ActionClient(self, Land, "land")
        self._cli_prec_land = ActionClient(self, PrecisionLand, "precision_land")
        self._cli_drop_mode = ActionClient(self, DropMode, "drop_mode")
        self._cli_enable_motors = self.create_client(Trigger, "drop/enable_motors")
        self._cli_enable_drop_detect = self.create_client(Trigger, "drop/enable_detect")
        self._cli_disable_drop_detect = self.create_client(Trigger, "drop/disable_detect")
        self._cli_drop_drone = self.create_client(Trigger, "/drop_drone")
        self._cli_shift_down = self.create_client(Trigger, "/shift_down")
        self._cli_servo_command = self.create_client(ServoCommand, "/servo_command")

    def send_arm_takeoff(self, altitude: float) -> Future:
        """Sends arm and takeoff command to drone.

        Args:
            altitude (float): height to takeoff (m)

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_arm_takeoff.wait_for_server(timeout_sec=self._timeout_sec):
            self.get_logger().error("No action server available")
            return
        goal = ArmTakeoff.Goal(altitude=altitude)
        self.get_logger().info(f"Sending goal to `arm_takeoff`")
        future = self._cli_arm_takeoff.send_goal_async(goal, feedback_callback=self._feedback_arm_takeoff)
        future.add_done_callback(functools.partial(self._action_response, "arm_takeoff"))
        return future

    def send_land(self) -> Future:
        """Sends land command to drone.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_land.wait_for_server(timeout_sec=self._timeout_sec):
            self.get_logger().error("No action server available")
            return
        goal = Land.Goal()
        self.get_logger().info(f"Sending goal to `land`")
        future = self._cli_land.send_goal_async(goal)
        future.add_done_callback(functools.partial(self._action_response, "land"))
        return future

    def send_precision_land(self, source_frame, target_frame) -> Future:
        """Sends precision land command to drone.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_prec_land.wait_for_server(timeout_sec=self._timeout_sec):
            self.get_logger().error("No action server available")
            return
        goal = PrecisionLand.Goal()
        goal.source_frame = source_frame
        goal.target_frame = target_frame
        self.get_logger().info(f"Sending goal to `precision_land`")
        future = self._cli_prec_land.send_goal_async(goal, feedback_callback=self._feedback_prec_land)
        future.add_done_callback(functools.partial(self._action_response, "precision_land"))
        return future

    def send_drop_mode(self) -> Future:
        """Sends drop mode command to drone.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_prec_land.wait_for_server(timeout_sec=self._timeout_sec):
            self.get_logger().error("No action server available")
            return
        goal = DropMode.Goal()
        self.get_logger().info(f"Sending goal to `drop_mode`")
        future = self._cli_drop_mode.send_goal_async(goal, feedback_callback=self._feedback_drop_mode)
        future.add_done_callback(functools.partial(self._action_response, "drop_mode"))
        return future
    
    def send_drop_enable_motors(self) -> Future:
        """Sends enable motors to drone. Only if drop mode has been aborted/cancelled.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_enable_motors.wait_for_service(timeout_sec=self._timeout_sec):
            self.get_logger().error("No service available")
            return
        req = Trigger.Request()
        future = self._cli_enable_motors.call_async(req)
        self._executor.spin_until_future_complete(future)
        return future.result()
    
    def send_drop_enable_detect(self) -> Future:
        """Sends enable drop detect flag to drone. Allows for drop mode detection to start.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_enable_drop_detect.wait_for_service(timeout_sec=self._timeout_sec):
            self.get_logger().error("No service available")
            return
        req = Trigger.Request()
        future = self._cli_enable_drop_detect.call_async(req)
        self._executor.spin_until_future_complete(future)
        return future.result()
    
    def send_drop_disable_detect(self) -> Future:
        """Sends disable drop detect flag to drone. Stops drop mode detections.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_disable_drop_detect.wait_for_service(timeout_sec=self._timeout_sec):
            self.get_logger().error("No service available")
            return
        req = Trigger.Request()
        future = self._cli_disable_drop_detect.call_async(req)
        self._executor.spin_until_future_complete(future)
        return future.result()

    def send_drop_drone(self) -> Future:
        """Sends drop_drone to carrier drone. Opens the bottom servo.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_drop_drone.wait_for_service(timeout_sec=self._timeout_sec):
            self.get_logger().error("No service available")
            return
        req = Trigger.Request()
        future = self._cli_drop_drone.call_async(req)
        self._executor.spin_until_future_complete(future)
        return future.result()
    
    def send_shift_down(self) -> Future:
        """Sends shift_down to carrier drone. Opens each servo other than the bottom.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_shift_down.wait_for_service(timeout_sec=self._timeout_sec):
            self.get_logger().error("No service available")
            return
        req = Trigger.Request()
        future = self._cli_shift_down.call_async(req)
        self._executor.spin_until_future_complete(future)
        return future.result()
    
    def send_servo_command(self, command: str) -> Future:
        """Sends servo command to drone.

        Args:
            commandl (str): Servo command string, must be binary length 4. 1 = open, 0 = close. Index corresponds to servo top-down.

        Returns:
            Future: a Future instance to a goal handle that completes when the handle has been
                accepted or rejected
        """
        self.reset()
        if not self._cli_servo_command.wait_for_service(timeout_sec=self._timeout_sec):
            self.get_logger().error("No service available")
            return
        req = ServoCommand.Request(command=command)
        future = self._cli_servo_command.call_async(req)
        self._executor.spin_until_future_complete(future)
        return future.result()
    
    #######################
    ## Feedback callbacks
    #######################
    def _feedback_arm_takeoff(self, feedback):
        self.get_logger().info(f"`arm_takeoff` feedback: {feedback.feedback.distance}m", throttle_duration_sec=2.0)

    def _feedback_prec_land(self, feedback):
        self.get_logger().info(f"`precision_land` feedback: {feedback.feedback.distance}m, {feedback.feedback.stage}", throttle_duration_sec=2.0)

    def _feedback_drop_mode(self, feedback):
        self.get_logger().info(f"`drop_mode` feedback: {feedback.feedback.velocity}m/s, {feedback.feedback.acceleration}m/s^2", throttle_duration_sec=2.0)
