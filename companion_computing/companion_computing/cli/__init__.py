'''
Client interfaces for sending commands to vehicles. Uses ROS2 to send actions and goals. Also,
contains a shell to easily control through custom terminal.
'''
