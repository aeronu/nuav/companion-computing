#!/usr/bin/env python
'''
Basic Drone
=======================

Contains the main flight algorithm parent class BasicDrone and all subsequent child classes.

ROS2 Actions
--------------------
- :code:`~/arm_takeoff` with msg ArmTakeoff_
- :code:`~/land` with msg Land_
- :code:`~/precision_land` with msg PrecisionLand_
- :code:`~/recover_aruco` with msg RecoverAruco_

.. _ArmTakeoff: https://aeronu.gitlab.io/nuav/companion-computing/api/companion_computing_interfaces/action/ArmTakeoff.html
.. _Land: https://aeronu.gitlab.io/nuav/companion-computing/api/companion_computing_interfaces/action/Land.html
.. _PrecisionLand: https://aeronu.gitlab.io/nuav/companion-computing/api/companion_computing_interfaces/action/PrecisionLand.html
.. _RecoverAruco: https://aeronu.gitlab.io/nuav/companion-computing/api/companion_computing_interfaces/action/RecoverAruco.html
'''
# Global packages
import sys
import numpy as np
from argparse import ArgumentParser
from companion_computing.utils.structs import Frame
from simple_pid import PID
from enum import IntEnum

# Local packages 
from companion_computing.basic_vehicle import BasicVehicle

# ROS messages, services, and actions
from companion_computing_interfaces.action import ArmTakeoff, Land, PrecisionLand, DropMode, RecoverAruco, GoToCoordinate
from companion_computing_interfaces.msg import Coordinate
from px4_msgs.msg import VehicleCommand, NuavDrop, NuavDropAck
from aruco_ros_interfaces.srv import AboveAruco
from geometry_msgs.msg import Twist, Pose, Point, Transform
from std_msgs.msg import Int64MultiArray, Bool
from std_srvs.srv import Trigger

# Downloaded packages
import rclpy
from rclpy.action import ActionServer, CancelResponse, ActionClient
from rclpy.time import Time
from rclpy.duration import Duration
from rclpy.executors import MultiThreadedExecutor
from tf2_ros.transform_listener import TransformListener
from tf2_ros.buffer import Buffer
from rcl_interfaces.msg import ParameterDescriptor, ParameterType
from scipy.spatial.transform import Rotation as R


class PrecLandStage(IntEnum):
    QUICK=0  # no limitations
    HORIZONTAL=1
    YAW=2
    DESCENT=3


class BasicDrone(BasicVehicle):       # Parent drone class
    def __init__(self, log_level="info", instance=0):
        """Initializes and connects drone to ROS and PX4.

        Args:
            log_level (str, optional): string for changing node log level. Defaults to "info".
            instance (int, optional): Instance of PX4 SITL. Defaults to 0.
        """        
        ################
        ## Setup ROS
        ################
        super().__init__(log_level, instance) # start node
        
        ################
        ## Setup Nodes, Publishers, Subscribers, Services, and Actions
        ################
        # Nodes
        self._precision_node = rclpy.create_node('basic_vehicle_tf_node', use_global_arguments=False)  # needed to access /tf to get around the namespace of drone_0

        # Actions
        self._server_arm_takeoff = ActionServer(self, ArmTakeoff, "arm_takeoff", self._handle_arm_takeoff_goal,
            cancel_callback=self._handle_arm_takeoff_cancel)
        self._server_land = ActionServer(self, Land, "land", self._handle_land_goal,
            cancel_callback=self._handle_land_cancel)
        self._server_precision_land = ActionServer(self, PrecisionLand, "precision_land", self._handle_precision_land_goal,
            cancel_callback=self._handle_precision_land_cancel)
        self._server_drop_mode = ActionServer(self, DropMode, "drop_mode", self._handle_drop_goal,
            cancel_callback=self._handle_drop_cancel)

        # Publishers
        self._pub_velocity = self.create_publisher(Twist, 'prec_land/velocity', 10)
        self._pub_drop = self.create_publisher(NuavDrop, f"{self.rtps_namespace}/NuavDrop_PubSubTopic", 10)

        # Subscribers
        self.create_subscription(NuavDropAck, f"{self.rtps_namespace}/NuavDropAck_PubSubTopic", self._callback_drop_ack, 1)
        self.create_subscription(Bool, f"gpio_state", self._callback_gpio_state, 1)

        self._server_recover_aruco = ActionServer(self, RecoverAruco, "recover_aruco", self._handle_recover_aruco_goal,
            cancel_callback=self._handle_recover_aruco_cancel)
        
        self._client_go_to = ActionClient(self, GoToCoordinate, 'go_to_coordinate')

        # Services
        self._above_aruco_client = self.create_client(AboveAruco, 'above_aruco')
        self._enable_motor_service = self.create_service(Trigger, 'drop/enable_motors', self._handle_drop_enable_motors)
        self._enable_drop_detect_service = self.create_service(Trigger, 'drop/enable_detect', self._handle_drop_enable_detect)
        self._disable_drop_detect_service = self.create_service(Trigger, 'drop/disable_detect', self._handle_drop_disable_detect)
        
        ################
        ## Setup Parameters
        ################
        self.declare_parameter('pland.yaw.p', 0.95)
        self.declare_parameter('pland.yaw.i', 0.05)
        self.declare_parameter('pland.yaw.d', 0.1)
        self.declare_parameter('pland.yaw.max', 90.0)

        self.declare_parameter('pland.xy.p', 0.4)
        self.declare_parameter('pland.xy.i', 0.05)
        self.declare_parameter('pland.xy.d', 0.1)
        self.declare_parameter('pland.xy.max', 0.5)

        self.declare_parameter('pland.alignment.height', 1.0,
            descriptor=ParameterDescriptor(description="Height to care about alignment.",
                type=ParameterType.PARAMETER_DOUBLE))
        self.declare_parameter('pland.alignment.settling_speed', 0.1)
        self.declare_parameter('pland.alignment.tolerance.xy', 0.09)  # from solidworks
        self.declare_parameter('pland.alignment.tolerance.yaw', 15.0)  # from solidworks
        self.declare_parameter('pland.alignment.funnel.xy', 0.3,
            descriptor=ParameterDescriptor(description="Scalar for funneling xy tolerance.",
                type=ParameterType.PARAMETER_DOUBLE))

        self.declare_parameter('pland.z.p', 0.075)
        self.declare_parameter('pland.z.i', 0.0)
        self.declare_parameter('pland.z.d', 0.0)
        self.declare_parameter('pland.z.max', 1.0)

        self.declare_parameter('pland.timeout', 2.0)
        self.declare_parameter('pland.cutoff_height', 0.3)

        # Drop thresholds
        self.declare_parameter('drop.vz', 1.5)
        self.declare_parameter('drop.az', 1.0)
        self.declare_parameter('drop.detect.default_flag', False)

        self.declare_parameter('pland.type', 'velocity')
    
        ################
        ## Setup TF2
        ################
        self.tf2_bf = Buffer()
        self.tf2_tfl = TransformListener(buffer=self.tf2_bf, node=self._precision_node)

        ################
        ## Setup private variables
        ################
        self._in_prec_land = False
        self._nuav_drop_ack = None
        self._drop_detect_flag = False

    ################
    ## Helper functions
    ################
    def land(self):
        """Method to land vehicle.
        """        
        self.get_logger().info("Landing Vehicle")
        self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_NAV_LAND)  # Send MAV_CMD_NAV_LAND_LOCAL
        # TODO: figure out how to visualize landing point on ground without knowing where ground is
        self._reset_offboard_changes(msg="Landing resetting offboard changes!")  # Forcefully stop publishing setpoints
        self.set_setpoint()  # Remove previous visualization point
        
    def _send_disable_motor_command(self, disable_motors, timeout=None):
        """Helper function to send the disable_motors msg to PX4.

        Args:
            disable_motors (bool): True to disable the motors, False to enable the motors.
            timeout (float, optional): Time in seconds to wait for ack msg from px4

        Returns:
            bool: Flag indicating successful send of command.
        """
        start_time = self.get_clock().now().nanoseconds/1.0e9
        self._nuav_drop_ack = None
            
        while True:
            # fail if timeout is reached
            if timeout:
                curr_time = self.get_clock().now().nanoseconds/1.0e9
                dt = curr_time - start_time
                if dt >= timeout:
                    self.publish_error("Timeout: motors ack not received")
                    return False
            
            # Wait for ack to be received
            if not self._nuav_drop_ack:
                self._pub_drop.publish(NuavDrop(disable_motors=disable_motors))
                continue
            elif self._nuav_drop_ack.result == NuavDropAck.DROP_DISABLE_ACK and not disable_motors:
                self.publish_info(f"Motors enabled")
                self._nuav_drop_ack = None
                return True
            elif self._nuav_drop_ack.result == NuavDropAck.DROP_ENABLE_ACK and disable_motors:
                self.publish_info(f"Motors disabled")
                self._nuav_drop_ack = None
                return True
                
    ################
    ## Callback Functions
    ################
    def _callback_drop_ack(self, msg):
        self._nuav_drop_ack = msg
        
    def _callback_gpio_state(self, msg):
        # if msg.data and self._in_prec_land and self.is_armed():
        if msg.data and self.is_armed():
            self.publish_info("GPIO detected: sending kill command.")
            self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, 0.0, 21196) #Forces a dissarm

    ################
    ## Service handlers
    ################
    def _handle_drop_enable_detect(self, req, res):
        self.publish_info("Drop mode detection enabled")
        self._drop_detect_flag = True
        res.success = True
        return res
    
    def _handle_drop_disable_detect(self, req, res):
        self.publish_info("Drop mode detection disabled")
        self._drop_detect_flag = False
        res.success = True
        return res
        
    def _handle_drop_enable_motors(self, req, res):
        self.publish_error("This is for testing only: comment me to enable")
        res.success = False
        return res
        
        res.success = self._send_disable_motor_command(disable_motors=False, timeout=2.0)
        return res
    
    ################
    ## Action handlers
    ################
    def _handle_drop_goal(self, goal):
        """Method to handle drop mode.
        """
        
        # Helper functions 
        def _abort_callback(msg):
            
            self.publish_error(msg)
            self._drop_detect_flag = False
            goal.abort()
            
        def _safety_checks():
            if not self._drop_detect_flag:
                _abort_callback(msg="DropMode: detection flag is off")
                return False
            if goal.is_cancel_requested:
                goal.canceled()
                return False
            if self.local_position is None:
                _abort_callback(msg="DropMode: local position is not set")
                return False
            return True
            
        feedback_msg = DropMode.Feedback()
        vz_thresh = self.get_parameter('drop.vz').value
        az_thresh = self.get_parameter('drop.az').value
        self._drop_detect_flag = self.get_parameter('drop.detect.default_flag').value
        if self._drop_detect_flag:
            self.publish_warn("DropMode: detect flag enabled by default")

        # Disable motors
        self._send_disable_motor_command(True)
        
        # Creating a setpoint above current position and changing to offboard
        self.set_coordinate(x=0.0, y=0.0, z=-2.0, frame=Coordinate.FRD)

        # Force Arm
        if not self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, 1.0, 21196.0):
            self._pub_drop.publish(NuavDrop(disable_motors=False))
            _abort_callback(msg="DropMode: force arm failed")
            return DropMode.Result()

        self.publish_info("DropMode: waiting for detection flag...") 
        while not self._drop_detect_flag:
            if goal.is_cancel_requested:
                goal.canceled()
                return DropMode.Result()
        
        # Wait for start of drop
        self.publish_info(f"DropMode: waiting for drop with az {az_thresh}m/s^2 and vz {vz_thresh}m/s...") 
        while True:
            if not _safety_checks():
                return DropMode.Result()       
            if self.local_position.az >= az_thresh:
                break
            feedback_msg.velocity = float(self.local_position.vz)
            feedback_msg.acceleration = float(self.local_position.az)
            goal.publish_feedback(feedback_msg)
        
        # Wait for speed threshold
        while True:
            if not _safety_checks():
                return DropMode.Result()     
            if self.local_position.vz >= vz_thresh:
                break
            feedback_msg.velocity = float(self.local_position.vz)
            feedback_msg.acceleration = float(self.local_position.az)
            goal.publish_feedback(feedback_msg)

        #Enable motors to catch
        self.publish_info("DropMode: detected!!!")
        self.set_coordinate(x=0.0, y=0.0, z=-0.2, frame=Frame.FRD)
        self._send_disable_motor_command(False)

        # Wait for catch
        while self.local_position.vz > 0.5:
            # Pilot override handling
            if not self.in_offboard():
                _abort_callback(msg=f"DropMode: mode was changed from OFFBOARD to {self.get_nav_state_name()}")
                return DropMode.Result()
            self.set_setpoint(x=0.0, y=0.0, z=-0.2, frame=Frame.FRD)
            feedback_msg.velocity = float(self.local_position.vz)
            feedback_msg.acceleration = -999.0
            goal.publish_feedback(feedback_msg)

        goal.succeed()
        self._drop_detect_flag = False
        return DropMode.Result()

    def _handle_drop_cancel(self, cancel):
        """Callback for cancelling drop mode.
        """
        self.publish_warn("Drop mode action canceled! Make sure motors are re-enabled")
        self.disarm(force=True)
        self._reset_offboard_changes(msg="Cancelling Drop Mode has reset offboard")
        self._drop_detect_flag = False
        return CancelResponse.ACCEPT

    def _handle_precision_land_goal(self, goal):
        pland_type = self.get_parameter('pland.type').value
        if pland_type == "velocity":
            return self._handle_precision_land_goal_vel(goal)
        elif pland_type == "position":
            return self._handle_precision_land_goal_pos(goal)
        else:
            raise ValueError(f"Invalid pland type: {pland_type}")

    def _handle_precision_land_goal_vel(self, goal):
        self.publish_info(f"PrecisionLanding on {goal.request.target_frame} with velocities")

        yaw_p = self.get_parameter('pland.yaw.p').value
        yaw_i = self.get_parameter('pland.yaw.i').value
        yaw_d = self.get_parameter('pland.yaw.d').value
        yaw_max = self.get_parameter('pland.yaw.max').value

        xy_p = self.get_parameter('pland.xy.p').value
        xy_i = self.get_parameter('pland.xy.i').value
        xy_d = self.get_parameter('pland.xy.d').value
        xy_max = self.get_parameter('pland.xy.max').value

        z_p = self.get_parameter('pland.z.p').value
        z_i = self.get_parameter('pland.z.i').value
        z_d = self.get_parameter('pland.z.d').value
        z_max = self.get_parameter('pland.z.max').value

        #PID
        # TODO: should the x and y PIDs be in body frame are world frame? does it matter?
        pid_x = PID(Kp=xy_p, Ki=xy_i, Kd=xy_d, output_limits=(-xy_max, xy_max))
        pid_y = PID(Kp=xy_p, Ki=xy_i, Kd=xy_d, output_limits=(-xy_max, xy_max))
        pid_z = PID(Kp=z_p, Ki=z_i, Kd=z_d, output_limits=(-z_max, z_max))
        pid_yaw = PID(Kp=yaw_p, Ki=yaw_i, Kd=yaw_d, output_limits=(-np.deg2rad(yaw_max), np.deg2rad(yaw_max)))

        if goal.request.source_frame == "":
            source_frame = self.namespace
        else:
            source_frame = goal.request.source_frame
        if self._handle_precision_land_preloop(goal): return PrecisionLand.Result()

        # Feedback and stages loop
        last_transform = None
        stage = PrecLandStage.QUICK
        while rclpy.ok():
            rclpy.spin_once(self._precision_node)
            if self._handle_precision_land_loop(goal, last_transform): return PrecisionLand.Result()
            try:
                curr_tf = self._get_tf(source_frame, goal.request.target_frame)
            except Exception as e:
                continue
            last_transform = Time.from_msg(curr_tf.header.stamp)
            curr_yaw = get_tf_yaw(curr_tf)

            #send translation velocity
            twist_msg = Twist()
            stage = self._get_precision_land_stage(curr_tf, stage)
            if stage == PrecLandStage.QUICK:
                twist_msg.linear.x = float(pid_x(-curr_tf.transform.translation.x))
                twist_msg.linear.y = float(pid_y(curr_tf.transform.translation.y))
                twist_msg.linear.z = float(pid_z(curr_tf.transform.translation.z))
                twist_msg.angular.z = float(pid_yaw(curr_yaw))
            elif stage == PrecLandStage.HORIZONTAL:
                twist_msg.linear.x = float(pid_x(-curr_tf.transform.translation.x))
                twist_msg.linear.y = float(pid_y(curr_tf.transform.translation.y))
            elif stage == PrecLandStage.YAW:
                twist_msg.angular.z = float(pid_yaw(curr_yaw))
            elif stage == PrecLandStage.DESCENT:
                twist_msg.linear.z = float(pid_z(curr_tf.transform.translation.z))
                if self._handle_precision_land_success(curr_tf, goal): return PrecisionLand.Result()
            else:
                raise ValueError(f"Not a valid precision land stage: {stage.name}")
            self._publish_precision_land_feedback(goal, curr_tf, stage)
            self.set_velocity(twist_msg.linear.x, twist_msg.linear.y, twist_msg.linear.z,twist_msg.angular.z)

        self.publish_error("PrecisionLand: exited the loop")
        goal.abort()
        self.set_velocity(0.0,0.0,0.0,0.0)
        self._in_prec_land = False
        return PrecisionLand.Result()
    
    def _handle_precision_land_cancel(self, cancel):
        """Callback for cancelling precision land.
        """        
        self.get_logger().info("PrecisionLand action canceled!")
        self._pub_velocity.publish(Twist())#send empty twist to stop velocity
        self._in_prec_land = False
        return CancelResponse.ACCEPT

    def _get_precision_land_stage(self, tf: Transform, prev_stage: PrecLandStage):
        align_height = self.get_parameter('pland.alignment.height').value
        align_settle_speed = self.get_parameter('pland.alignment.settling_speed').value
        align_tol_yaw = np.deg2rad(self.get_parameter('pland.alignment.tolerance.yaw').value)
        align_tol_xy = self.get_parameter('pland.alignment.tolerance.xy').value
        funnel_xy = self.get_parameter('pland.alignment.funnel.xy').value
        cutoff_height = self.get_parameter('pland.cutoff_height').value

        curr_yaw = get_tf_yaw(tf)
        xy_norm = np.linalg.norm([tf.transform.translation.x, tf.transform.translation.y])
        z = -tf.transform.translation.z  # TODO: shouldn't need to flip z here, get tf differently
        if z >= align_height:  # OUTSIDE ALIGNMENT STAGE
            if prev_stage != PrecLandStage.QUICK:
                self.publish_info(f"PrecisionLand: exiting alignment stages at {z:.3f}m, switching PID")
            return PrecLandStage.QUICK
        else:
            if not prev_stage in [PrecLandStage.HORIZONTAL, PrecLandStage.YAW, PrecLandStage.DESCENT]:
                self.publish_info(f"PrecisionLand: entered alignment stages at {z:.3f}m, switching PID")
            funnel_shift = max(0.0, z-cutoff_height)*funnel_xy
            shifted_tol_xy = align_tol_xy + funnel_shift
            if (self.settled(align_settle_speed) and xy_norm < shifted_tol_xy):  # WITHIN HORIZONTAL TOLERANCE
                if not prev_stage in [PrecLandStage.YAW, PrecLandStage.DESCENT]:
                    self.publish_info(f"PrecisionLand: aligned with horizontal tolerance at {shifted_tol_xy:.3f}m")
                if abs(curr_yaw) < align_tol_yaw:  # WITHIN YAW TOLERANCE
                    if prev_stage != PrecLandStage.DESCENT:
                        self.publish_info(f"PrecisionLand: aligned with yaw tolerance at {abs(curr_yaw):.3f}rads")
                    return PrecLandStage.DESCENT
                else:
                    if prev_stage != PrecLandStage.YAW:
                        self.publish_info(f"PrecisionLand: lost alignment with yaw tolerance at {abs(curr_yaw):.3f}rads")
                    return PrecLandStage.YAW
            else:  # OUTSIDE TOLERANCES
                if prev_stage != PrecLandStage.HORIZONTAL:
                    self.publish_info(f"PrecisionLand: lost alignment with horizontal tolerance at {shifted_tol_xy:.3f}m")
                return PrecLandStage.HORIZONTAL

    def _handle_precision_land_success(self, curr_tf, goal):
        cutoff_height = self.get_parameter('pland.cutoff_height').value
        if -curr_tf.transform.translation.z < cutoff_height: #TODO possibly average or make sure its there for a while?
            self.publish_info(f"Precision landed!")
            self.land()
            goal.succeed()
            self._in_prec_land = False
            return True
        return False

    def _handle_precision_land_loop(self, goal, last_transform):
        timeout = self.get_parameter('pland.timeout').value
        # Failure and cancel handling
        if not self.in_offboard() and last_transform:
            self.get_logger().error(f"PrecisionLand: mode was changed from OFFBOARD to {self.get_nav_state_name()}")
            goal.abort()
            return True
        if goal.is_cancel_requested:
            goal.canceled()
            self._in_prec_land = False
            return True
        #check for timeout
        curr_time = self.get_clock().now().nanoseconds/1.0e9
        dt = curr_time - (last_transform.nanoseconds/1.0e9) if last_transform is not None else None
        if dt is not None and dt > timeout:
            self.publish_error(f"PrecisionLand action aborted due to a stale transform. {dt:.2f} seconds old.") #TODO pass result as to why it aborted?
            goal.abort()
            self.halt()
            self.set_velocity(0.0,0.0,0.0,0.0)
            self._in_prec_land = False
            return True
        return False

    def _handle_precision_land_preloop(self, goal):
        #sanity check that we are armmed
        if not self.is_armed():
            self.publish_info("Vehicle already landed...")
            goal.succeed()
            return True
        #parameters
        self._in_prec_land = True
        return False

    def _publish_precision_land_feedback(self, goal, curr_tf, stage):
        feedback_msg = PrecisionLand.Feedback()
        feedback_msg.distance = float(abs(curr_tf.transform.translation.z))
        feedback_msg.stage = stage.name
        goal.publish_feedback(feedback_msg)

    def _get_tf(self, source_frame, target_frame):
        try:
            curr_tf = self.tf2_bf.lookup_transform(self.namespace, target_frame, Time())
            # Make offset
            if source_frame != self.namespace:
                offset_tf = self.tf2_bf.lookup_transform(self.namespace, source_frame, Time(), timeout=Duration(seconds=4))####source_frame then target_frame
                curr_tf.transform.translation.x -= offset_tf.transform.translation.x
                curr_tf.transform.translation.y += offset_tf.transform.translation.y
                curr_tf.transform.translation.z += offset_tf.transform.translation.z
            return curr_tf
        except Exception as e: #possibly tf2.LookupException or tf2.ExtrapolationException
            self.get_logger().error(f"{str(e)}", throttle_duration_sec=1.0)
            raise e

    def _handle_precision_land_goal_pos(self, goal):
        self.publish_info(f"PrecisionLanding on {goal.request.target_frame} with positions")
        z_factor = 0.1         

        if goal.request.source_frame == "":
            source_frame = self.namespace
        else:
            source_frame = goal.request.source_frame
        if self._handle_precision_land_preloop(goal): return PrecisionLand.Result()

        # Feedback and stages loop
        last_transform = None
        stage = PrecLandStage.QUICK
        while rclpy.ok():
            rclpy.spin_once(self._precision_node)
            if self._handle_precision_land_loop(goal, last_transform): return PrecisionLand.Result()
            try:
                curr_tf = self._get_tf(source_frame, goal.request.target_frame)
            except Exception as e:
                continue
            last_transform = Time.from_msg(curr_tf.header.stamp)
            curr_yaw = get_tf_yaw(curr_tf)

            # send positional command
            point_msg = Point()
            pose_yaw = 0.0
            xy_norm = np.linalg.norm([curr_tf.transform.translation.x, curr_tf.transform.translation.y])
            stage = self._get_precision_land_stage(curr_tf, stage)
            if stage == PrecLandStage.QUICK:
                point_msg.x = float(curr_tf.transform.translation.x)
                point_msg.y = float(-curr_tf.transform.translation.y)
                point_msg.z = float(-curr_tf.transform.translation.z)*z_factor
                pose_yaw = float(curr_yaw)
            elif stage == PrecLandStage.HORIZONTAL:
                point_msg.x = curr_tf.transform.translation.x
                point_msg.y = -curr_tf.transform.translation.y
            elif stage == PrecLandStage.YAW:
                pose_yaw = curr_yaw
            elif stage == PrecLandStage.DESCENT:
                point_msg.z = float(-curr_tf.transform.translation.z)*z_factor
                if self._handle_precision_land_success(curr_tf, goal): return PrecisionLand.Result()
            else:
                raise ValueError(f"Not a valid precision land stage: {stage.name}")
            self._publish_precision_land_feedback(goal, curr_tf, stage)
            self.set_coordinate(point_msg.x, point_msg.y, point_msg.z, pose_yaw, frame=Frame.FRD)

        self.publish_error("PrecisionLand: exited the loop")
        goal.abort()
        self.set_coordinate(0.0, 0.0, 0.0, 0.0, Frame.FRD)
        self._in_prec_land = False
        return PrecisionLand.Result()
    
    def _handle_precision_land_cancel_pos(self, cancel):
        """Callback for cancelling precision land.
        """        
        self.get_logger().info("PrecisionLand action canceled!")
        self.set_coordinate(0.0, 0.0, 0.0, 0.0, Frame.FRD)
        self._in_prec_land = False
        return CancelResponse.ACCEPT

    def _handle_arm_takeoff_goal(self, goal):
        """Action callback for arming and taking off vehicle.
        """
        # Check that necessary internal vars are set
        if self.global_position is None:
            self.publish_error("ArmTakeoff: global position must be set")
            goal.abort()
            return ArmTakeoff.Result()

        # TODO: Should this abort or succeed? do we need this?
        # Succeed if we are already armed
        if self.is_armed() and not self._vehicle_landed.landed:
            self.publish_warn("ArmTakeoff: already armed.")
            goal.succeed()
            return ArmTakeoff.Result()

        global_alt = self.global_position.alt + goal.request.altitude  # Need to use MSL for takeoff
        takeoff_mode = self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_NAV_TAKEOFF, param7=global_alt)
        if not takeoff_mode:
            self.publish_warn("ArmTakeoff: couldn't switch to takeoff")
            goal.abort()
            return ArmTakeoff.Result()
        relative_alt = self._odometry.z + -1*goal.request.altitude
        self.set_setpoint(x=self._odometry.x, y=self._odometry.y, z=relative_alt)

        if not self.arm():
            self.publish_warn("ArmTakeoff: couldn't arm, aborting")
            goal.abort()
            return ArmTakeoff.Result()

        self.publish_info('ArmTakeoff: success, taking off')
        
        # Check to see if vehicle reaches takeoff altitude and handle case if goal is cancelled by forcing a landing
        feedback_msg = ArmTakeoff.Feedback()
        rate = self.create_rate(20)
        while True:
            rate.sleep()
            distance = self.distance_to_target()    
            reached = self.reached_target(distance=distance)        
            if distance is not None: #publish distance to target
                feedback_msg.distance = float(distance)
                goal.publish_feedback(feedback_msg)
            if goal.is_cancel_requested:
                goal.canceled() #handle cancel action
                return ArmTakeoff.Result()
            if reached:
                self.publish_info("ArmTakeoff: reached destination")
                goal.succeed() #handle sucess
                return ArmTakeoff.Result()

    def _handle_arm_takeoff_cancel(self, cancel):
        """Callback for cancelling arm_takeoff action. Must be used with ActionServer.
        """
        self.land()
        self.publish_info(f'ArmTakeoff: cancelling takeoff...')
        while not self._vehicle_landed.landed:
            continue
        self.publish_info(f'ArmTakeoff: action cancelled!')
        return CancelResponse.ACCEPT

    def _handle_land_goal(self, goal):
        """Action callback to land vehicle.
        """        
        self.land()
        rate = self.create_rate(20)
        while True:
            rate.sleep()
            if goal.is_cancel_requested:
                goal.canceled()
                return Land.Result()
            if self._vehicle_landed.landed:
                goal.succeed()
                return Land.Result()

    def _handle_land_cancel(self, cancel):
        """Callback for cancelling land action. Must be used with ActionServer.
        """
        self.publish_info('Land: cancelling...')
        self.halt()
        self.publish_info('Land: canceled!')
        return CancelResponse.ACCEPT

    def _handle_recover_aruco_goal(self, goal):
        """Action callback to search for aruco array.
        """
        #TODO: for now probably just do GotoHorizontal, where you stay at altitude and goto original GPS land coord 
        self.get_logger().info(f'RecoverAruco: called {goal.request}')
        
        # send go to coordinate action request
        goal_msg = GoToCoordinate.Goal()
        goal_msg.coordinate = goal.request.coordinate
        future = self._client_go_to.send_goal_async(goal_msg, feedback_callback=self._handle_recover_aruco_feedback)

        # constantly check for aruco
        rate = self.create_rate(20)
        while True:
            self.get_logger().info(f'searching...', throttle_duration_sec=2.0)
            rate.sleep()
            settled = self.settled()
            distance = self.distance_to_target()    
            reached = self.reached_target(distance=distance, tolerance=1.0)  # TODO: check for future completion instead since go to coordinate action implements this

            # call AboveAruco request
            id_array = Int64MultiArray()
            id_array.data = range(0,5)
            request = AboveAruco.Request()
            request.ids_to_check = id_array
            result = self._above_aruco_client.call(request)
            if result.above:
                self.get_logger().info("RecoverAruco: found!")
                self._client_go_to._cancel_goal(future.result())
                goal.succeed()
                return RecoverAruco.Result()
                
            # if settled and near goal point we abort because aruco not found
            if settled and reached:
                self.get_logger().warn(f'RecoverAruco: aborting, settled and aruco not found')
                goal.abort()
                return RecoverAruco.Result()

    def _handle_recover_aruco_feedback(self, feedback_msg):
        self.get_logger().debug(f'RecoverAruco Feedback: {feedback_msg.feedback}')

    def _handle_recover_aruco_cancel(self, cancel):
        """Callback for cancelling recover aruco action. Must be used with ActionServer.
        """
        self.get_logger().info('ArucoRecover: cancelling...')
        return CancelResponse.ACCEPT

def get_tf_yaw(tf):
    r = R.from_quat([tf.transform.rotation.x, tf.transform.rotation.y, tf.transform.rotation.z, tf.transform.rotation.w])
    return r.as_euler('xyz', degrees=False)[2]

################
## Main
################
def main(args=None):
    rclpy.init(args=args)
    
    # Setup argument parsing
    parser = ArgumentParser()
    parser.add_argument("--log-level", default='info', choices=["info", "debug", "warn", "error", "fatal"], help='ros log level')
    parser.add_argument("-i", "--instance", default=0, type=int, help="Instance of vehicle.")
    args, _ = parser.parse_known_args()

    drone = BasicDrone(log_level=args.log_level, instance=args.instance)
    
    executor = MultiThreadedExecutor()
    rclpy.spin(drone, executor=executor)


if __name__ == '__main__':
    main()
