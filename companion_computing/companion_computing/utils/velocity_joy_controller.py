#! /usr/bin/env python
"""
Velocity Joy Controller
=======================
Controls vehicle with joy stick controller.

"""

from tf2_ros.transform_broadcaster import TransformBroadcaster
import rclpy
from rclpy.node import Node

from geometry_msgs.msg import PoseStamped, Vector3, Quaternion, TransformStamped, Twist
from sensor_msgs.msg import Joy
from std_msgs.msg import Header
from tf2_msgs.msg import TFMessage
from companion_computing_interfaces.action import ArmTakeoff
from companion_computing_interfaces.srv import StateReport

from rclpy.action import ActionClient

from argparse import ArgumentParser

class VelocityJoyController(Node):
    def __init__(self, namespace="drone_0", log_level=None):
        super().__init__('position_joy_controller', namespace=namespace)
        if log_level is not None:
            self.get_logger().set_level(log_level)
        self.create_subscription(Joy, "/joy", self.joy_callback, 10)

        # Create client to waypoint action on separate node
        self.takeoff_node = rclpy.create_node("joy_takeoff", namespace=namespace)
        self.state_node = rclpy.create_node("joy_state", namespace=namespace)
        self.arm_takeoff_action_client = ActionClient(self.takeoff_node, ArmTakeoff, "arm_takeoff")
        self.state_client = self.state_node.create_client(StateReport, "state")

        self.pub_velocity = self.create_publisher(Twist, "cmd/velocity", 1)

        # Initialize local vars
        self.max_vel = 0.1
        self.namespace = namespace

        self.get_logger().info(
            f"Started position joy controller for {self.namespace}!\n"
            f"Press A on XBOX to takeoff\n"
            f"Left joystick is for Z velocity and yaw\n"
            f"Right joystick is for X and Y velocity\n"
            f"Right Trigger for boosted velocity output")

    def joy_callback(self, msg):
        """
        Tell drone to move and send new waypoint based on controller input.
        
        sudo apt install ros-foxy-joy
        axes[0] - left joystick side to side (1.0 : -1.0)
        axes[1] - left joystick up to down (1.0 : -1.0)
        axes[2] - left trigger (1.0 : -1.0)
        axes[3, 4, 5] - same as left side
        """
        left_joy_x_axis = msg.axes[0]
        left_joy_y_axis = msg.axes[1]
        right_joy_x_axis = msg.axes[3]
        right_joy_y_axis = msg.axes[4]

        try:
            # self.get_logger().info("Trying to get state")
            while not self.state_client.wait_for_service(timeout_sec=1.0):
                self.get_logger().info('State service not available, waiting again...')
            req = StateReport.Request()
            future = self.state_client.call_async(req)
            rclpy.spin_until_future_complete(self.state_node, future)
            state_report = future.result()
            # self.get_logger().info(f"Current state {state_report.state}", throttle_duration_sec=1.0)
        except Exception as e: #replaced rospy with rclpy, unsure if this works
            self.get_logger().error("Service call failed: %s" % e)
            return

        # Press A to Takeoff
        if msg.buttons[0] and state_report.state == "REST":
            goal = ArmTakeoff.Goal()
            goal.altitude = 1.0
            try:
                future = self.arm_takeoff_action_client.send_goal_async(goal)
                rclpy.spin_until_future_complete(self.takeoff_node, future)
            except Exception as e:
                self.get_logger().warning('Service call failed %r' % (e,))
            self.tookoff = True
        elif state_report.state != "REST":
            if abs(right_joy_x_axis) < 0.1 and abs(right_joy_y_axis) < 0.1 and abs(left_joy_y_axis) < 0.1:
                return

            max_vel_boosted = 5.0
            max_vel = max(self.max_vel, max_vel_boosted * ((-msg.axes[5] + 1.0)/2.0))

            max_yaw_rate = 3.14159/8.0

            # Calculate next position based on right joystick movement
            x = max_vel * right_joy_y_axis
            y = max_vel * -right_joy_x_axis
            z = max_vel * -left_joy_y_axis

            vel = Twist()
            vel.linear.x = x
            vel.linear.y = y
            vel.linear.z = z
            if abs(left_joy_x_axis) > 0.1:
                vel.angular.z = -left_joy_x_axis * max_yaw_rate

            self.pub_velocity.publish(vel)
        

def main(args=None):
    rclpy.init(args=args)                # Set rospy logging level
    
    # Setup argument parsing
    parser = ArgumentParser()
    parser.add_argument("-ns", "--namespace", default=None, type=str, help="Namespace that all ROS nodes are prefixed with. Also, name of drone.")
    parser.add_argument("--log-level", default='info', choices=["info", "debug", "warn", "error", "fatal"], type=str, help='ros log level')

    args, _ = parser.parse_known_args()
    
    controller = VelocityJoyController(namespace=args.namespace)
    
    rclpy.spin(controller)


if __name__ == '__main__':
    main()

        
