#!/usr/bin/env python
'''
Camera
======

ROS2 node for listening to and publishing a GPIO pin state.
'''

import rclpy
from rclpy.node import Node
from rclpy.logging import get_logging_severity_from_string
from std_msgs.msg import Bool

import RPi.GPIO as GPIO


class GPIOListener(Node):
    def __init__(self, log_level='info'):
        """ GPIO Listener node to publish the state of a GPIO pin.
        
        Args:
            log_level (str, optional): string for changing node log level. Defaults to "info".
        """
        super().__init__('gpio_listener')
        if log_level is not None:
            self.get_logger().set_level(get_logging_severity_from_string(log_level))
        self.namespace = self.get_namespace().split("/")[-1]
        
        # Setup publisher
        self.gpio_publisher = self.create_publisher(Bool, "gpio_state", 10)
        
        timer_period = 1/100  # seconds
        self.timer = self.create_timer(timer_period, self._update)
        
        # Declare and read parameters
        if not self.has_parameter("input_pin"): self.declare_parameter("input_pin", 18)  # BCM pin 18, BOARD pin 12
        self.input_pin = self.get_parameter("input_pin").get_parameter_value().integer_value
        
        # Setup GPIO
        GPIO.setmode(GPIO.BCM) 
        GPIO.setup(self.input_pin, GPIO.IN, GPIO.PUD_DOWN)
        
    def _update(self):
        """Checks the state of a given GPIO pin and publishes it to the topic gpio_state.
        """
        gpio_state = GPIO.input(self.input_pin)
        
        msg = Bool()
        msg.data = bool(gpio_state)
        self.gpio_publisher.publish(msg)
        
        
def main(args=None):
    rclpy.init(args=args)
    gpio = GPIOListener()
    rclpy.spin(gpio)
    
    gpio.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
