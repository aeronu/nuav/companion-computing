#!/usr/bin/env python3
'''
Bag Plot
=========

Pulls information from ROS2 bag and plots. Currently only supports plotting 3D position.
'''
import sqlite3
from rosidl_runtime_py.utilities import get_message
from rclpy.serialization import deserialize_message
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import argparse
import os
from pathlib import Path


class BagFileParser():
    def __init__(self, bag_file: str):
        """Reads a bag file and parses it using SQL.

        Args:
            bag_file (str): Path to ROS2 sqllite database file.
        """
        self.con = sqlite3.connect(bag_file)
        self.cursor = self.con.cursor()

        ## create a message type map
        topics_data = self.cursor.execute("SELECT id, name, type FROM topics").fetchall()
        self.topic_type = {name_of:type_of for id_of,name_of,type_of in topics_data}
        self.topic_id = {name_of:id_of for id_of,name_of,type_of in topics_data}
        self.topic_msg_message = {name_of:get_message(type_of) for id_of,name_of,type_of in topics_data}

    def __del__(self):
        self.con.close()

    # Return [(timestamp0, message0), (timestamp1, message1), ...]
    def get_messages(self, topic_name):
        """Gets messages from a topic.

        Args:
            topic_name (str): Topic name to get messages from.

        Returns:
            list: List of tuples with format (timestampe, msg).
        """
        topic_id = self.topic_id[topic_name]
        # Get from the db
        rows = self.cursor.execute("SELECT timestamp, data FROM messages WHERE topic_id = {}".format(topic_id)).fetchall()
        # Deserialise all and timestamp them
        return [ (timestamp,deserialize_message(data, self.topic_msg_message[topic_name])) for timestamp,data in rows]


class PlotFunctions:
    def __init__(self, bag_file: str, output_dir: str):
        self.bag_file = str(bag_file)
        self.output_dir = str(output_dir)
        self.parser = BagFileParser(self.bag_file)

    # Plot the pose of the drone
    def pose3D(self, pose_topic, title, output):
        """Plots 3D position using topic name.

        Args:
            pose_topic (str, optional): Pose topic to read messages from.
        """
        # Initializing x,y,z lists and plot scale
        x = []
        y = []
        z = []
        plot_scale = 15

        # Gets messages from the pose topic
        loc = self.parser.get_messages(pose_topic)

        # Appends each x,y,z value to respective list. 
        # Sets all axes to the max x,y, or z value found 
        for timestamp, message in loc:
            x_val = message.pose.position.x
            y_val = message.pose.position.y
            z_val = message.pose.position.z

            x.append(x_val)
            y.append(y_val)
            z.append(z_val)
            plot_scale = max(x_val,y_val,z_val)
        
        # Generates plot
        plot = plt.figure()
        axes = plt.axes(projection='3d')
        axes.plot3D(x,y,z,'black')
        axes.auto_scale_xyz([0,plot_scale],[0,plot_scale],[0,plot_scale])
        axes.set_title(title)
        plot.savefig(os.path.join(self.output_dir, output))


def argument_parser():
    parser = argparse.ArgumentParser(description= 'Generates a 3D plot')

    parser.add_argument('function', type=str, choices=['pose3D'], default='pose3D', help='Type of graph to generate')
    parser.add_argument('bag_file', type=str, default='filename.bag',help='Input bag file')
    parser.add_argument('-o','--output-dir', type=str, default=str(Path.home()), help='Output directory')
    parser.add_argument("-ns", "--namespace", type=str, default="drone_0", help="ROS namespace for topic")

    args = parser.parse_args()
    return args


def main():
    options = argument_parser()
    plot = PlotFunctions(options.bag_file, options.output_dir)
    namespace = options.namespace

    if options.function == "pose3D":
        plot.pose3D(f"{namespace}/pose", f"{namespace} pose", f"{namespace}_pose_3d.png")


if __name__ == "__main__":
    main()
