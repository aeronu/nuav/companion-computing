'''
Keyboard Control
=================
Controls vehicle with keyboard inputs.

===== ==============
Input Output
===== ==============
w     +x
----- --------------
a     +y
----- --------------
s     -x
----- --------------
d     -y
----- --------------
q     +z
----- --------------
e     -z
----- --------------
t     takeoff
----- --------------
y     land
----- --------------
z     \* 1.1 to speed
----- --------------
x     \* 0.9 to speed
===== ==============
'''
from typing import final
import rclpy
from rclpy.node import Node
from rclpy.action import ActionClient
from geometry_msgs.msg import Twist
import sys, select, termios, tty
from companion_computing_interfaces.action import ArmTakeoff
from companion_computing_interfaces.action import Land
from companion_computing_interfaces.srv import StateReport

settings = termios.tcgetattr(sys.stdin)

moveBindings = {
    'w':(1,0,0,0),
    'a':(0,1,0,0),
    's':(-1,0,0,0),
    'd':(0,-1,0,0),
    'q': (0,0,-1,0),
    'e': (0,0,1,0),
    'o' : (0,0,0,0)
}
speedBindings = {
    'z': (1.1, 1.1),
    'x': (0.9, 0.9)
}


def getKey():
    tty.setraw(sys.stdin.fileno())
    select.select([sys.stdin], [], [], 0)
    key = sys.stdin.read(1)
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key

def main(args=None):
    if args is None:
        args = sys.argv

    rclpy.init()
    node = rclpy.create_node('takeoff', namespace="drone_0")
    pub_velocity = node.create_publisher(Twist, "/drone_0/cmd/velocity", 10)
    arm_takeoff_action_client = ActionClient(node, ArmTakeoff, "arm_takeoff")
    arm_land_action_client = ActionClient(node, Land, "land")
    state_client = node.create_client(StateReport, "state")
    speed = 0.5
    turn = 1.0
    th = 0.0

    x = 0.0
    y = 0.0
    z = 0.0

    try:
        while(True):
            key = getKey()
            if key in moveBindings.keys():
                print(key)
                x = moveBindings[key][0]
                y = moveBindings[key][1]
                z = moveBindings[key][2]
            elif key == 't':
                goal = ArmTakeoff.Goal()
                goal.altitude = 5.0
                try:
                    future = arm_takeoff_action_client.send_goal_async(goal)
                    rclpy.spin_until_future_complete(node, future)
                except Exception as e:
                    print(e)
            elif key == 'y':
                goal = Land.Goal()
                try : 
                    future = arm_land_action_client.send_goal_async(goal)
                    rclpy.spin_until_future_complete(node, future)
                except Exception as e:
                    print(e)
            elif key in speedBindings.keys():
                speed = speed * speedBindings[key][0]
                turn = turn * speedBindings[key][1]
            else:
                x = 0.0
                y = 0.0
                z = 0.0
                if (key == '\x03'):
                    break
            twist = Twist()
            twist.linear.x = x * speed; twist.linear.y = y * speed; twist.linear.z = z * speed
            twist.angular.x = 0.0 ; twist.angular.y = 0.0 ; twist.angular.z = th * turn
            pub_velocity.publish(twist)
    except Exception as e:
        print(e)
    finally:
        twist = Twist()
        twist.linear.x = 0.0 ; twist.linear.y = 0.0 ; twist.linear.z = 0.0
        twist.angular.x = 0.0 ; twist.angular.y = 0.0 ; twist.angular.z = 0.0
        pub_velocity.publish(twist)


if __name__ == '__main__':
    main()