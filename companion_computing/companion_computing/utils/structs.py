'''
Structs
========
Useful enums for vehicles.
'''
from enum import IntEnum
from companion_computing_interfaces.msg import Coordinate


class Frame(IntEnum):
    """Coordinate frame reference."""
    LLA = Coordinate.LLA
    LOCAL_NED = Coordinate.LOCAL_NED
    FRD = Coordinate.FRD


class PX4LogLevel(IntEnum):
    """Log level from PX4."""
    EMERGENCY=0
    ALERT=1
    CRITICAL=2
    ERROR=3
    WARNING=4
    NOTICE=5
    INFO=6
    DEBUG=7


class PX4Mode(IntEnum):
    """Mode for PX4."""
    HOLD=0
    OFFBOARD=1
    STABILIZED=2
    ACRO=5
    POSITION=6


class State(IntEnum):
    REST = 0
    RUNNING = 1
    STANDBY = 2
