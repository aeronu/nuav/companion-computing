'''
Drone TF Broadcaster
=====================
Transforms for drone sensors and position.
'''
#! /usr/bin/env python
from numpy.lib.ufunclike import fix
from tf2_ros.transform_broadcaster import TransformBroadcaster
from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
import rclpy
from rclpy.node import Node
from rclpy.logging import get_logging_severity_from_string

from geometry_msgs.msg import PoseStamped, Vector3, Quaternion, TransformStamped
from std_msgs.msg import Header
from tf2_msgs.msg import TFMessage
from scipy.spatial.transform import Rotation as R

from argparse import ArgumentParser
import numpy as np


class DroneTfBroadcaster(Node):
    """Class to broadcast predefined TF frames for parts of the drone as we wait for ros2_control.
    """    
    def __init__(self, namespace='', x=0.0, y=0.0, z=0.15, log_level='info', camera_tilt=np.pi/2):
        """Initialize drone tf broadcaster ROS node.

        Args:
            namespace (str, optional): Drone's namespace in ROS. Defaults to ''.
            instance (int, optional): Instance of drone being used. Defaults to 0.
            log_level (str, optional): Log level for ROS node. Defaults to "info".
        """
        # NOTE: namespace is not passed to node so TF publishes to /tf
        super().__init__('drone_tf_broadcaster')
        self.get_logger().set_level(get_logging_severity_from_string(log_level))
            
        self.namespace = namespace

        # Create tf broadcaster object
        self.br = TransformBroadcaster(self)
        self.static_br = StaticTransformBroadcaster(self)

        # Create Subscriber to pose for updating drone tf frame
        self._sub_pose = self.create_subscription(PoseStamped, f'{self.namespace}/pose',    
                         self._handle_pose, 10)

        # Positions of all fixed sensors
        fixed_frames = []
        # NOTE: Keep these updated especially if positions are changed either in sim or on vehicle
        static_time = self.get_clock().now().to_msg()
        realsense_tf = TransformStamped()
        realsense_translation = Vector3(x=0.052, y=0.04, z=-0.0186)  # 0.052 0 -0.0186
        x, y, z, w = R.from_euler("xyz", [0, np.pi, camera_tilt]).as_quat()
        realsense_quat = Quaternion(x=x, y=y, z=z, w=w)
        realsense_tf.transform.translation = realsense_translation
        realsense_tf.transform.rotation = realsense_quat
        realsense_header = Header()
        realsense_header.stamp = static_time
        realsense_header.frame_id = self.namespace
        realsense_tf.header = realsense_header
        realsense_tf.child_frame_id = f"{self.namespace}/realsense"
        fixed_frames.append(realsense_tf)

        ir_camera_tf = TransformStamped()
        ir_header = Header()
        ir_header.stamp = static_time
        ir_header.frame_id = f"world"
        ir_camera_tf.header = ir_header
        ir_camera_tf.child_frame_id = f"{self.namespace}/realsense/camera_color_optical_frame"
        fixed_frames.append(ir_camera_tf)

        color_camera_tf = TransformStamped()
        color_header = Header()
        color_header.stamp = static_time
        color_header.frame_id = f"world"
        color_camera_tf.header = ir_header
        color_camera_tf.child_frame_id = f"{self.namespace}/realsense/camera_left_ir_optical_frame"
        fixed_frames.append(color_camera_tf)

        # Publish static initial position
        home_tf = TransformStamped()
        home_translation = Vector3(x=x, y=y, z=z)  # TODO: have way of setting home TF using home position of drone
        x, y, z, w = R.from_euler("xyz", [0, 0, np.pi/2]).as_quat()  # North as 90 degrees
        home_q = Quaternion(x=x, y=y, z=z, w=w)
        home_tf.transform.translation = home_translation
        home_tf.transform.rotation = home_q
        home_header = Header()
        home_header.stamp = static_time
        home_header.frame_id = "world"
        home_tf.header = home_header
        home_tf.child_frame_id = f"{self.namespace}/home"
        fixed_frames.append(home_tf)

        # TODO: make automatic converter for sdf to urdf with model paths
        # # For robot state publisher
        # base_footprint_tf = TransformStamped()
        # header = Header()
        # header.stamp = static_time
        # header.frame_id = "drone_0"
        # base_footprint_tf.header = header
        # base_footprint_tf.child_frame_id = "base_footprint"
        # fixed_frames.append(base_footprint_tf)

        self.static_br.sendTransform(fixed_frames)

    def _handle_pose(self, msg):
        """ROS subscriber to republish drone's position in TF.

        Args:
            msg (PoseStamped): ROS pose message.
        """        
        # Construct transform message
        tfmsg = TransformStamped()
        translation = Vector3(x=msg.pose.position.x, y=msg.pose.position.y, z=msg.pose.position.z)
        tfmsg.transform.translation = translation
        tfmsg.transform.rotation = msg.pose.orientation
        tfmsg.child_frame_id = self.namespace
        tfmsg.header = msg.header
        tfmsg.header.frame_id = f"{self.namespace}/home"
        self.br.sendTransform(tfmsg)


def main(args=None):
    rclpy.init(args=args)
    
    # Setup argument parsing
    parser = ArgumentParser()
    parser.add_argument("-ns", "--namespace", default='', type=str, help="Namespace that all ROS nodes are prefixed with. Also, name of drone.")
    parser.add_argument("-x", default=0.0, type=float, help="x position")
    parser.add_argument("-y", default=0.0, type=float, help="y position")
    parser.add_argument("-z", default=0.15, type=float, help="z position")
    parser.add_argument("--log-level", default='info', choices=["info", "debug", "warn", "error", "fatal"], type=str, help='ros log level')
    parser.add_argument("--camera-tilt", default=np.pi/2, type=float, help="Camera tilt for realsense.")

    args, _ = parser.parse_known_args()

    drone_tf_broadcaster = DroneTfBroadcaster(namespace=args.namespace, x=args.x, y=args.y, z=args.z, log_level=args.log_level, camera_tilt=args.camera_tilt)
    
    rclpy.spin(drone_tf_broadcaster)


if __name__ == '__main__':
    main()
    