# Global packages
from enum import Enum
import time

# Local packages
from companion_computing_interfaces.srv import ServoCommand

# ROS messages, services, and actions
from std_srvs.srv import Trigger

# Downloaded packages
import smbus
import rclpy
from rclpy.node import Node
from rclpy.logging import get_logging_severity_from_string

class ServoCommands(Enum):
    # Top to Bottom is index: 0,1,2,3
    # open = 1, close = 0
    
    CLOSE_ALL = "0000"
    OPEN_3 = "0001"
    OPEN_2 = "0100"
    OPEN_1 = "0010"
    OPEN_0 = "1000"


class I2CNode(Node):
    def __init__(self, log_level='info') -> None:
        ################
        ## Setup ROS
        ################
        super().__init__('i2c_controller')
        if log_level is not None:
            self.get_logger().set_level(get_logging_severity_from_string(log_level))
        self.namespace = self.get_namespace().split("/")[-1]
        
        ################
        ## Setup Nodes, Publishers, Subscribers, Services, and Actions
        ################
        #Setup service
        self._drop_drone_service = self.create_service(Trigger, 'drop_drone', self._handle_drop_drone)
        self._shift_drone_service = self.create_service(Trigger, 'shift_down', self._handle_shift_down)
        self._servo_command_service = self.create_service(ServoCommand, 'servo_command', self._handle_servo_command)

        ################
        ## Setup private variables
        ################
        self._bus = smbus.SMBus(1)
        
        if not self.has_parameter("i2c_address"): self.declare_parameter("i2c_address", 0x08)
        self.ADDRESS = self.get_parameter("i2c_address").get_parameter_value().integer_value
        
    
    ################
    ## Helper functions
    ################
    def send_i2c_cmd(self, cmd, sleep_seconds=1.0):
        cmd_encoded = list(cmd.value.encode())
        self._bus.write_i2c_block_data(self.ADDRESS, cmd_encoded[0], cmd_encoded[1:])
        time.sleep(sleep_seconds)
    
    ################
    ## Service handlers
    ################
    def _handle_shift_down(self, req, res):
        self.get_logger().info("Shifting down")
        self.send_i2c_cmd(ServoCommands.OPEN_2)        
        self.send_i2c_cmd(ServoCommands.CLOSE_ALL)        
        self.send_i2c_cmd(ServoCommands.OPEN_1)        
        self.send_i2c_cmd(ServoCommands.CLOSE_ALL)        
        self.send_i2c_cmd(ServoCommands.OPEN_0)   
        self.send_i2c_cmd(ServoCommands.CLOSE_ALL, sleep_seconds=0.0)               
        res.success = True
        return res
    
    def _handle_drop_drone(self, req, res):        
        self.get_logger().info("Dropping drone")
        self.send_i2c_cmd(ServoCommands.OPEN_3)        
        self.send_i2c_cmd(ServoCommands.CLOSE_ALL, sleep_seconds=0.0)        
        res.success = True
        return res
    
    def _handle_servo_command(self, req, res):
        if len(req.command) != 4 or not all(c in '01' for c in req.command):
            res.success = False
            res.message = "Servo command must be a string of 4 0's or 1's"
            return res
        
        #Correct for servo/index mismatch
        cmd_arr = list(req.command)
        cmd_arr[1], cmd_arr[2] = cmd_arr[2], cmd_arr[1]
        cmd = ''.join(cmd_arr)
        
        # send command
        self.get_logger().info(f"Sending {req.command}")
        cmd_encoded = list(cmd.encode())
        self._bus.write_i2c_block_data(self.ADDRESS, cmd_encoded[0], cmd_encoded[1:])
        
        res.success = True
        return res
    
        
def main(args=None):
    rclpy.init(args=args)
    i2c_node = I2CNode()
    rclpy.spin(i2c_node)
    
    i2c_node.destroy_node()
    rclpy.shutdown()
    

if __name__ == '__main__':
    main()