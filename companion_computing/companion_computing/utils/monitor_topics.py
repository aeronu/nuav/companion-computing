'''
Montior Topics
===============
Publishes all topic names to a topic.
'''
import rclpy
from rclpy.node import Node
from rclpy.logging import get_logging_severity_from_string
from companion_computing_interfaces.msg import Topics, Topic

TIMER_PERIOD = 0.5

class TopicPublisher(Node):
    """Node that publishes topics actively broadcasting."""
    def __init__(self, namespace=None, log_level='info'):
        """Outputs a topic with the names and message types of all topics under a namespace.

        Args:
            namespace (str, optional): Namespace for ROS topics. Defaults to None.
            log_level (str, optional): Log level for ROS. Defaults to 'info'.
        """
        super().__init__('topic_publisher', namespace=namespace)
        if log_level is not None:
            self.get_logger().set_level(get_logging_severity_from_string(log_level))

        self.topic_publisher = self.create_publisher(Topics, "topics", 1)
        self.timer = self.create_timer(TIMER_PERIOD, self.publish_topics)
        self.namespace = self.get_namespace().split("/")[-1]

    def publish_topics(self):
        """Publish topics."""
        topic_list = self.get_topic_names_and_types()
        topics_msg = Topics()

        topic_msgs = []
        for topic in topic_list:
            if self.namespace != topic[0].split("/")[1]:
                continue
            topic_msg = Topic()
            topic_msg.topic = topic[0]
            topic_msg.types = topic[1]
            topic_msgs.append(topic_msg)

        topics_msg.topics = topic_msgs

        self.topic_publisher.publish(topics_msg)




def main(args=None):
    rclpy.init(args=args)

    topic_publisher = TopicPublisher()

    rclpy.spin(topic_publisher)
    rclpy.shutdown()

if __name__ == "__main__":
    main()