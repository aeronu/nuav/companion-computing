#!/usr/bin/env python
'''
Basic Plane
===============

Contains the main flight algorithm parent class BasicPlane
'''
# Global packages
from argparse import ArgumentParser

# Local packages
from companion_computing.basic_vehicle import BasicVehicle

# Ros packages
import rclpy
from rclpy.executors import MultiThreadedExecutor


class BasicPlane(BasicVehicle):       # Parent rover class
    def __init__(self, log_level='info', instance=0):
        """Initializes and connects plane to ROS and PX4.

        Args:
            log_level (str, optional): string for changing node log level. Defaults to "info".
            instance (int, optional): Instance of PX4 SITL. Defaults to 0.
        """        
        ################
        ## Setup ROS
        ################
        super().__init__(log_level, instance) # start node
        

def main(args=None):
    rclpy.init(args=args)                # Set rospy logging level
    
    # Setup argument parsing
    parser = ArgumentParser()
    parser.add_argument("--log-level", default='info', choices=["info", "debug", "warn", "error", "fatal"], help='ros log level')
    parser.add_argument("-i", "--instance", default=0, type=int, help="Instance of vehicle.")
    args, _ = parser.parse_known_args()

    plane = BasicPlane(log_level=args.log_level, instance=args.instance)
    
    executor = MultiThreadedExecutor()
    rclpy.spin(plane, executor=executor)


if __name__ == '__main__':
    main()
