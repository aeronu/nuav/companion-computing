#!/usr/bin/env python
'''
Basic Vehicle
=======================

Contains generic class representing a px4 vehicle

ROS2 Actions
------------
- :code:`~/go_to_coordinate` with msg GoToCoordinate_
- :code:`~/go_to_coordinates` with msg FollowCoordinates_

.. _GoToCoordinate: https://aeronu.gitlab.io/nuav/companion-computing/api/companion_computing_interfaces/action/GoToCoordinate.html
.. _FollowCoordinates: https://aeronu.gitlab.io/nuav/companion-computing/api/companion_computing_interfaces/action/FollowCoordinates.html

ROS2 Services
-------------
- :code:`~/kill` with msg Trigger_
- :code:`~/arm` with msg Trigger_
- :code:`~/disarm` with msg Trigger_
- :code:`~/set_offboard` with msg Trigger_

.. _Trigger: https://docs.ros2.org/latest/api/std_srvs/srv/Trigger.html
'''
# Global packages
import numpy as np
import navpy
from scipy.spatial.transform import Rotation as R
from companion_computing.utils.structs import PX4LogLevel, PX4Mode, Frame, State
from companion_computing.utils.log_node import LogNode

# ROS messages, services, and actions
from companion_computing_interfaces.msg import PositionalTelemetry, Mavlink, RPY, VehiclePosition, Parameters
from companion_computing_interfaces.action import GoToCoordinate, FollowCoordinates
from companion_computing_interfaces.srv import StateReport
from px4_msgs.msg import (
    TrajectorySetpoint, HomePosition, VehicleStatus, VehicleOdometry, VehicleCommand, Timesync, OffboardControlMode, BatteryStatus, 
    VehicleLandDetected, VehicleGlobalPosition, VehicleGpsPosition, VehicleLocalPosition, VehicleCommandAck, LogMessage
)
from std_msgs.msg import Header, String, Float32
from geometry_msgs.msg import PoseStamped, Twist, PointStamped, Point
from nav_msgs.msg import Odometry
from std_srvs.srv import Trigger
from sensor_msgs.msg import NavSatFix, BatteryState
from rcl_interfaces.msg import Log

# ROS libraries
import rclpy
from rclpy.action import ActionServer, CancelResponse
from rclpy.qos import QoSProfile
from rclpy.logging import get_logging_severity_from_string
from rclpy.callback_groups import ReentrantCallbackGroup
from rclpy.executors import MultiThreadedExecutor
from rclpy.node import Node
from rclpy.clock import Duration


class BasicVehicle(LogNode):
    def __init__(self, log_level='info', instance=0):
        """Initializes and connects vehicle to ROS and PX4.

        Args:
            log_level ([type], optional): log level. Defaults to "info".
            instance (int, optional): instance for PX4 SITL. Defaults to 0.
        """        
        ################
        ## Setup 
        ################
        super().__init__("basic_vehicle", "log") # start node
        if log_level is not None:
            self.get_logger().set_level(get_logging_severity_from_string(log_level))
        self._default_callback_group = ReentrantCallbackGroup()  # ROS processes need to be run in parallel for this use case
        
        #Call update function every 20ish seconds
        timer_period = 1/100  # seconds
        self.timer = self.create_timer(timer_period, self._update)
        
        # FIXME: tmp flag for publishing setpoints when trying to change to offboard
        self._changing_offboard = False

        #Set variables
        self.namespace = self.get_namespace().split("/")[-1]
        self.rtps_namespace = "RTPS"
        self.instance = instance
        self._setpoint_count = 0
        self._max_setpoints = 21  # Amount of setpoints to send before allowing offboard switch
        self._prev_reset_time = self.get_clock().now()  # Keeps track of last time offboard was attempting to set
        self._reset_wait_time = Duration(seconds=20.0)  # Time to wait before resetting offboard mode
        # Wait times for methods
        self.offboard_wait_time = Duration(seconds=20.0)
        # Callback storage
        self._home_coordinate = None #TODO getter and setter
        self._mode = VehicleStatus() #TODO getter and setter
        self._odometry = None
        self._global_position = None
        self._local_position = None
        self._gps_position = None
        self._battery_status = BatteryStatus()
        self._rtps_timestamp = 0
        self._vehicle_landed = VehicleLandDetected()
        self._vehicle_command_ack = VehicleCommandAck()
        self._vehicle_command_ack.target_system = self.instance + 1
        self._vehicle_command_ack.target_component = 1
        # Main position setpoint that needs to be published constantly
        self._trajectory_setpoint = TrajectorySetpoint()
        # Offboard params that needs to be published constantly
        self._offboard_mode = OffboardControlMode()
        self._offboard_mode.position = True
        self._offboard_mode.velocity = False
        self._offboard_mode.acceleration = False
        self._offboard_mode.attitude = False
        self._offboard_mode.body_rate = False

        # Parameter logging (maybe move to inherited class like LogNode)
        self._pub_parameters = self.create_publisher(Parameters, "parameters", 1)
        self._published_parameters_once = False

        #Wait for a vehicle connection
        while self.count_publishers(f"{self.rtps_namespace}/Timesync_PubSubTopic") <= 0:
            self.get_logger().info(f"Waiting for vehicle with name '{self.namespace}'...", throttle_duration_sec=5.0)
        
        ################
        ## Setup Publishers, Subscribers, Services, and Actions
        ################
        # Publishers
        self._pub_vehicle_telemetry = self.create_publisher(PositionalTelemetry, "telemetry", 10)
        self._pub_current_target = self.create_publisher(PointStamped, "current_target", 10)
        self._pub_pose = self.create_publisher(PoseStamped, "pose", 10) # These are for visualizing in rviz
        self._pub_odom = self.create_publisher(Odometry, "odom", 10)    # These are for visualizing in rviz
        #ROSboard publishers
        self._pub_hdop = self.create_publisher(Float32, "hdop", 10)
        self._pub_nav_state = self.create_publisher(String, "nav_state", 10)
        self._pub_rpy = self.create_publisher(RPY, "rpy", 10) 
        self._pub_px4_log = self.create_publisher(Log, "px4_log", 10) 
        self._pub_battery = self.create_publisher(BatteryState, "battery_state", 10) 
        self._pub_navsatfix = self.create_publisher(NavSatFix, "navsatfix", 10)
        self._pub_vehicle_position = self.create_publisher(VehiclePosition, "/vehicle_position", 10) 
        # uOrb Publishers
        self._pub_trajectory_setpoint = self.create_publisher(TrajectorySetpoint, f"{self.rtps_namespace}/TrajectorySetpoint_PubSubTopic", 1)
        self._pub_vehicle_command = self.create_publisher(VehicleCommand, f"{self.rtps_namespace}/VehicleCommand_PubSubTopic", 1)
        self._pub_offboard_mode = self.create_publisher(OffboardControlMode, f"{self.rtps_namespace}/OffboardControlMode_PubSubTopic", 1)

        # Subscribers
        self._sub_vel  = self.create_subscription(Twist, "cmd/velocity", self._callback_velocity, 1)
        self._sub_mavlink = self.create_subscription(Mavlink, "cmd/mavlink", self._callback_mavlink, 1)
        # uOrb Subscribers
        self._sub_mode = self.create_subscription(VehicleStatus, f"{self.rtps_namespace}/VehicleStatus_PubSubTopic", self._callback_mode, 1)
        self._sub_home = self.create_subscription(HomePosition, f"{self.rtps_namespace}/HomePosition_PubSubTopic", self._callback_home_coordinate, 1)
        self._sub_odom = self.create_subscription(VehicleOdometry, f"{self.rtps_namespace}/VehicleOdometry_PubSubTopic", self._callback_odometry, 1)
        self._sub_timesync = self.create_subscription(Timesync, f"{self.rtps_namespace}/Timesync_PubSubTopic", self._callback_rtps_timestamp, 1)
        self._sub_vehicle_land = self.create_subscription(VehicleLandDetected, f"{self.rtps_namespace}/VehicleLandDetected_PubSubTopic", self._callback_vehicle_land, 1)
        self._sub_battery = self.create_subscription(BatteryStatus, f"{self.rtps_namespace}/BatteryStatus_PubSubTopic", self._callback_battery, 1)
        self._sub_gps = self.create_subscription(VehicleGpsPosition, f"{self.rtps_namespace}/VehicleGpsPosition_PubSubTopic", self._callback_gps, 1)
        self._sub_global_position = self.create_subscription(VehicleGlobalPosition, f"{self.rtps_namespace}/VehicleGlobalPosition_PubSubTopic", self._callback_global_position, 1)
        self._sub_vehicle_command_ack = self.create_subscription(VehicleCommandAck, f"{self.rtps_namespace}/VehicleCommandAck_PubSubTopic", self._callback_vehicle_command_ack, 1)
        self._sub_log_message = self.create_subscription(LogMessage, f"{self.rtps_namespace}/LogMessage_PubSubTopic", self._callback_log_message, 1)
        self._sub_local_position = self.create_subscription(VehicleLocalPosition, f"{self.rtps_namespace}/VehicleLocalPosition_PubSubTopic", self._callback_local_position, 1)

        # Services
        self._kill_service = self.create_service(Trigger, "kill", self._handle_kill)
        self._arm_service = self.create_service(Trigger, "arm", self._handle_arm)
        self._disarm_service = self.create_service(Trigger, "disarm", self._handle_disarm)
        self._set_offboard_service = self.create_service(Trigger, "set_offboard", self._handle_set_offboard)
        self._state_service = self.create_service(StateReport, "state", self._handle_state) # TODO: Figure out if state needs to be maintained in vehicle

        #Actions
        self._target_action = ActionServer(self, GoToCoordinate, "go_to_coordinate", self._handle_coordinate_goal,
            cancel_callback=self._handle_coordinate_cancel, goal_service_qos_profile=QoSProfile(depth=1))
        self._target_action_series = ActionServer(self, FollowCoordinates, "follow_coordinates", self._handle_follow_coordinates_goal,
            cancel_callback=self._handle_coordinate_cancel, goal_service_qos_profile=QoSProfile(depth=1))

        ############
        ## Setup Parameters
        ############
        self.declare_parameter('settle_speed_thresh', 0.3)
        self.declare_parameter('tolerance_location', 0.7)

        self.get_logger().info("---Initialization Successful---")

    ################
    ## Helper functions
    ################
    def _update(self):        
        """Method that is called like a loop (ex. 20Hz) to update things such as the position setpoint
        """
        if not self._published_parameters_once:
            config = Parameters()
            config.parameters = [self._parameters[k].to_parameter_msg() for k in self._parameters.keys()]
            self._pub_parameters.publish(config)
            self._published_parameters_once = True
        self._publish_telemetry()
        self._publish_pose_odom_rpy()
        self._publish_target()
        # ROSboard
        self._publish_vehicle_position_navsatfix()
        self._publish_hdop()

        # Only publish setpoints when in offboard, publishing otherwise affect takeoff and land
        if self._changing_offboard:
            self._publish_setpoint()
            self._publish_offboard_mode()

            if self._setpoint_count < self._max_setpoints:
                self._setpoint_count += 1

        # Disable publishing setpoints when not in offboard
        if not self.in_offboard() and (self.get_clock().now() - self._prev_reset_time) > self._reset_wait_time and self._setpoint_count == self._max_setpoints:
            self._reset_offboard_changes(msg=f"Resetting offboard changes! Mode now {self.get_nav_state_name()}.")

    def in_offboard(self):
        """Returns whether or not the vehicle is in offboard mode

        Returns:
            bool: Whether or not the vehicle is in offboard mode
        """
        return self._mode.nav_state == VehicleStatus.NAVIGATION_STATE_OFFBOARD

    def is_armed(self):
        """Returns whether the vehicle is currently armed

        Returns:
            bool: Whether the vehicle is currently armed
        """
        return self._mode.arming_state == VehicleStatus.ARMING_STATE_ARMED

    def arm(self):
        """Sends arm commmand over mavlink.

        Returns:
            bool: Whether the arm was sent
        """
        if not (self._mode.arming_state == VehicleStatus.ARMING_STATE_INIT or self._mode.arming_state == VehicleStatus.ARMING_STATE_STANDBY or self._mode.arming_state == VehicleStatus.ARMING_STATE_ARMED):
            self.get_logger().error(f'Vehicle arming state is {self._mode.arming_state} (see VehicleStatus msg)')
            return False
        elif self._mode.arming_state == VehicleStatus.ARMING_STATE_ARMED:
            self.get_logger().debug("Vehicle already armed!")
            return True

        return self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, 1.0)

    def disarm(self, force=False):
        """Disarm
        """
        # TODO: add check to see if vehicle is already disarmed and don't send command in that case
        if not self._vehicle_landed.landed:
            self.get_logger().error(f'Vehicle is not landed, cannot disarm')
            return False
        return self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, 0.0)
        

    def set_offboard(self):
        """Sets to offboard mode.
        """       
        # TODO: this probably needs to be updated since send_vehicle_cmd loops until messages is acked
        self._changing_offboard = True
        #Put into offboard mode
        in_offboard = self.in_offboard()
        if not in_offboard:
            self._prev_reset_time = self.get_clock().now()
            # TODO: figure out if we need setpoint count, currently breaks speeding up in sim, maybe not necessary because of vehicle command wait
            # while self._setpoint_count < self._max_setpoints and self._changing_offboard:
            #     continue
            while not self.in_offboard() and (self.get_clock().now() - self._prev_reset_time) < self.offboard_wait_time and self._changing_offboard:
                self.set_mode(PX4Mode.OFFBOARD)  # TODO: use result from set mode to exit early
                self.get_logger().info(f"Changing to Offboard mode.", throttle_duration_sec=5.0)
            if not self._changing_offboard:
                self.get_logger().warn("Something cancelled setting offboard")
                return False
            if not self.in_offboard():
                t = self.get_clock().now() - self._prev_reset_time
                self.get_logger().warn(f"Timed out waiting for offboard to be set {t.to_msg()}")
                return False
            self.get_logger().info("Succeeded in setting offboard.")
        return True

    def set_coordinate(self, x:float=np.nan, y:float=np.nan, z:float=np.nan, heading:float=np.nan, frame:Frame=Frame.LOCAL_NED) -> bool:
        """Sends a coordinate for the vehicle to immediatly go to.

        Args:
            x (float, optional): x position setpoint. In m for NED and degrees for lat/long. Defaults to np.nan.
            y (float, optional): y position setpoint. In m for NED and degrees for lat/long. Defaults to np.nan.
            z (float, optional): z position setpoint. In m for NED and degrees for lat/long. Defaults to np.nan.
            heading (float, optional): yaw (only for multirotors), in rad [-PI..PI), NaN = hold current yaw. Defaults to np.nan.
            frame (Frame, optional): Specifies frame for position control. Defaults to Frame.LOCAL_NED.

        Returns:
            bool: flag indicating setpoint was set
        """
        self.get_logger().debug(f"Coordinate cmd: x:{x}, y:{y}, z:{z}, heading:{heading}, frame:{frame}")
        
        if not self.set_offboard():
            self.get_logger().error('Vehicle could not be set to OFFBOARD mode')
            return False

        flag = self.set_setpoint(x=x, y=y, z=z, yaw=heading, frame=frame)
        self._set_offboard_mode(position=True)
        return flag

    def set_velocity(self, vx:float=np.nan, vy:float=np.nan, vz:float=np.nan, yawspeed:float=np.nan, body_frame:bool=True) -> bool: #TODO: What happens if it is sent once and not updated? we should catch that
        """Sends a velocity setpoint.

        Args:
            vx (float, optional): X velocity in m/s. Defaults to np.nan.
            vy (float, optional): Y velocity in m/s. Defaults to np.nan.
            vz (float, optional): Z velocity in m/s. Defaults to np.nan.
            yawspeed (float, optional): Yaw speed in rad/s. Defaults to np.nan.
            body_frame (bool, optional): Flag that uses body NED when set. Defaults to True.

        Returns:
            bool: flag indicating if velocity setpoint was set
        """        
        # self.get_logger().debug(f"Velocity cmd: x:{vx}, y:{vy}, z:{vz}, yawspeed:{yawspeed}", throttle_duration_sec=0.5)
        if not self.set_offboard():
            self.get_logger().warning('Vehicle could not be set to OFFBOARD mode', throttle_duration_sec=0.5)
            return False

        frame = Frame.FRD if body_frame else Frame.LOCAL_NED
        flag = self.set_setpoint(vx=vx, vy=vy, vz=vz, yawspeed=yawspeed, frame=frame)
        self._set_offboard_mode(velocity=True)
        return flag
        
    def set_setpoint(self, x=np.nan, y=np.nan, z=np.nan, yaw=np.nan, vx=np.nan, vy=np.nan, vz=np.nan, 
                     yawspeed=np.nan, frame=Frame.LOCAL_NED):
        if self.local_position is None:
            self.publish_error("Local position is not set, can't set setpoint")
            return False
        # Assume velocity is being used if no positions are set and one velocity component is set
        use_velocity = np.isnan([x, y, z]).all() and np.isfinite([vx, vy, vz]).any()

        # Handle various frames and conversions
        local_position = self.local_position
        if frame == Frame.LLA:
            ned = np.asfarray(navpy.lla2ned(x, y, z, local_position.ref_lat, local_position.ref_lon, local_position.ref_alt))
            x = ned[0]
            y = ned[1]
            z = ned[2]
        elif frame == Frame.LOCAL_NED:
            x = x
        elif frame == Frame.FRD and use_velocity:
            # Convert velocity commands relative to vehicle orientation
            # TODO: Confirm if body NED frame includes pitch and roll of vehicle
            r = R.from_quat([self._odometry.q[1], self._odometry.q[2], self._odometry.q[3], self._odometry.q[0]])
            v = r.apply([vx, vy, vz])  # Transformed point
            vx = v[0]
            vy = v[1]
            vz = v[2]
        elif frame == Frame.FRD and not use_velocity:
            # Convert position commands relative to vehicle orientation
            # TODO: Confirm if body NED frame includes pitch and roll of vehicle
            r = R.from_quat([self._odometry.q[1], self._odometry.q[2], self._odometry.q[3], self._odometry.q[0]])
            p = r.apply([x, y, z])  # Transformed point
            x = local_position.x + p[0]
            y = local_position.y + p[1]
            z = local_position.z + p[2]
            euler = r.as_euler('xyz', degrees=False)
            yaw = euler[2] - yaw
        else:
            self.get_logger().error(f"Frame '{frame.name}' is not supported yet with {'velocity' if use_velocity else 'position'}. Not setting setpoint.", throttle_duration_sec=5.0)
            return False

        self._trajectory_setpoint = TrajectorySetpoint()
        self._trajectory_setpoint.x = x			    # local position setpoint in m in NED
        self._trajectory_setpoint.y = y			    # local position setpoint in m in NED
        self._trajectory_setpoint.z = z			    # local position setpoint in m in NED
        self._trajectory_setpoint.yaw = yaw			# yaw (only for multirotors), in rad [-PI..PI), NaN = hold current yaw
        self._trajectory_setpoint.vx = vx			# local velocity setpoint in m/s in NED
        self._trajectory_setpoint.vy = vy			# local velocity setpoint in m/s in NED
        self._trajectory_setpoint.vz = vz			# local velocity setpoint in m/s in NED
        self._trajectory_setpoint.yawspeed	= yawspeed	    # yawspeed (only for multirotors, in rad/s)
            
        return True

    def distance_to_target(self) -> float:     
        """Returns distance to the target coordinate

        Returns:
            float: Distance in m
        """
        if self.local_position is None:
            self.publish_error("Local position must be set, can't get distance")
            return np.nan
        local_position = self.local_position
        curent_coord = np.array([local_position.x, local_position.y, local_position.z]) 
        target_coord_NED = np.array([self._trajectory_setpoint.x, self._trajectory_setpoint.y, self._trajectory_setpoint.z]) 
        return np.linalg.norm(curent_coord-target_coord_NED)

    def reached_target(self, tolerance:float=None, distance:float=None) -> bool:      
        """Determines if we have reached the target

        Args:
            tolerance (float, optional): Metres of tolerance to say that a target has been reached. Defaults to `tolerance_location` ros2 parameter.
            distance (float, optional): Distance away from target. Defaults to vehicle distance away.

        Returns:
            bool: Whether the target has been reached
        """
        if tolerance is None:
            tolerance = self.get_parameter('tolerance_location').value
        return tolerance >= (self.distance_to_target() if distance is None else distance)

    def settled(self, speed_thresh:float=None) -> bool:    
        """Determines if the vehicle is settled at a location.

        Args:
            speed_thresh (float, optional): Threshold at which the speed is considered settled (m/s). Defaults to `settle_speed_thresh` ros2 parameter.

        Returns:
            bool: Whether the vehicle has settled.
        """        
        # TODO: check if acceleration is close to zero also
        if speed_thresh is None:
            speed_thresh = self.get_parameter('settle_speed_thresh').value
        speed = np.linalg.norm([self._odometry.vx, self._odometry.vy, self._odometry.vz])
        # self.get_logger().debug(f"Settling speed: {speed}", throttle_duration_sec=1.0)
        return speed < speed_thresh

    def halt(self):
        """Sets the set position to the current odometry.
        """
        # TODO: make this HOLD mode
        # self.set_coordinate(self._odometry.x, self._odometry.y, self._odometry.z)
        self.get_logger().debug("Halting!")
        self._reset_offboard_changes()
        # TODO: is this the behavior we want? when switching to hold it tends to backtrack to its position when it was switched which is problematic when the vehicle is moving fast since its momentum carries it a far distance from that point
        self.set_mode(PX4Mode.HOLD)

    def send_vehicle_cmd(self, command, param1:float=np.nan, param2:float=np.nan, 
                         param3:float=np.nan, param4=np.nan, param5=np.nan, param6=np.nan, 
                         param7=np.nan, wait=True) -> bool:
        """Send vehicle command over RTPS.

        Args:
            command (int): Command ID, as defined MAVLink by uint16 VEHICLE_CMD enum.
            param1 (float): Parameter 1, as defined by MAVLink uint16 VEHICLE_CMD enum.
            param2 (float, optional): Parameter 2, as defined by MAVLink uint16 VEHICLE_CMD enum. Defaults to None.
            param3 (float, optional): Parameter 3, as defined by MAVLink uint16 VEHICLE_CMD enum. Defaults to None.
            param4 (float, optional): Parameter 4, as defined by MAVLink uint16 VEHICLE_CMD enum. Defaults to None.
            param5 (float, optional): Parameter 5, as defined by MAVLink uint16 VEHICLE_CMD enum. Defaults to None.
            param6 (float, optional): Parameter 6, as defined by MAVLink uint16 VEHICLE_CMD enum. Defaults to None.
            param7 (float, optional): Parameter 7, as defined by MAVLink uint16 VEHICLE_CMD enum. Defaults to None.
            wait (bool, optional): Flag indicating if this function should wait for an ACK. Defaults to True.

        Returns:
            bool: Whether the command was sent and received successfully (if waiting)
        """        
        msg = VehicleCommand()
        msg.timestamp = self._rtps_timestamp
        msg.param1 = float(param1)
        msg.param2 = float(param2)
        msg.param3 = float(param3)
        msg.param4 = float(param4)
        msg.param5 = float(param5)
        msg.param6 = float(param6)
        msg.param7 = float(param7)
        msg.command = int(command)
        msg.target_system = self.instance + 1
        msg.target_component = 1
        msg.source_system = self.instance + 1
        msg.source_component = 1
        msg.from_external = True
        self._pub_vehicle_command.publish(msg)
        if not wait:
            return True

        wait_dur = 0.5 * 10**6 # seconds
        max_sends = 20  # maximum times to send command before returning false
        sends = 0
        curr_time = self._rtps_timestamp
        stale_times = 0
        max_stale_times = 10
        rate = self.create_rate(100)
        while True:
            rate.sleep()
            self.get_logger().info(f"Waiting for command {msg.command} to be acknowleged...", throttle_duration_sec=5.0)
            command = self._vehicle_command_ack.command
            target_system = self._vehicle_command_ack.target_system
            target_component = self._vehicle_command_ack.target_component
            timestamp = self._vehicle_command_ack.timestamp
            result = self._vehicle_command_ack.result
            # Count occurrences where time has not changed in this loop
            stale_times = stale_times + 1 if curr_time == self._rtps_timestamp else 0
            if stale_times >= 100:
                self.get_logger().error("Time is not being updated, loop lockup occurring")
                return False
            curr_time = self._rtps_timestamp
            # self.get_logger().debug(f"sent cmd: {msg.command}, sent time: {msg.timestamp}, cmd: {command}, tgs: {target_system}, tgc: {target_component}, time: {timestamp}, result: {result}", throttle_duration_sec=1.0)
            # Attempt to send command again after a period of time has passed
            if curr_time - msg.timestamp > wait_dur and sends < max_sends:
                sends += 1
                self.get_logger().debug(f"Republishing command {msg.command} since no response has been received! Sent {sends} times!")
                msg.timestamp = self._rtps_timestamp
                self._pub_vehicle_command.publish(msg)
                continue
            elif sends >= max_sends:
                self.get_logger().error(f"Tried to send command {max_sends} times but never received ack!")
                return False
            same_system = (msg.source_system == target_system and msg.source_component == target_component and msg.command == command and msg.timestamp < timestamp)
            if not same_system:
                self.get_logger().debug(f"Ack message is not from this request... from system {target_system} and component {target_component} at time {timestamp} with command {command}", throttle_duration_sec=5.0)
                continue
            if result == VehicleCommandAck.VEHICLE_RESULT_ACCEPTED:
                return True
            elif result == VehicleCommandAck.VEHICLE_RESULT_TEMPORARILY_REJECTED:
                self.get_logger().error(f"Command [{command}] temporarily rejected...")
                return False
            elif result == VehicleCommandAck.VEHICLE_RESULT_DENIED:
                self.get_logger().error(f"Command [{command}] denied!")
                return False
            elif result == VehicleCommandAck.VEHICLE_RESULT_UNSUPPORTED:
                self.get_logger().error(f"Command [{command}] not supported!")
                return False
            elif result == VehicleCommandAck.VEHICLE_RESULT_FAILED:
                self.get_logger().error(f"Command [{command}] failed!")
                return False
            elif result == VehicleCommandAck.VEHICLE_RESULT_IN_PROGRESS:
                self.get_logger().debug(f"Command [{command}] in progress...")
                continue
    
    def set_mode(self, mode:PX4Mode):
        """Set PX4 mode.

        Args:
            mode (PX4Mode): Mode to switch to.
        """
        result = False
        # TODO: extend this to support more vehicle commands, also add service for setting mode
        if mode == PX4Mode.OFFBOARD:
            result = self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_DO_SET_MODE, 1, 6)
        elif mode == PX4Mode.HOLD:
            result = self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_DO_SET_MODE, 1, 4, 3)
        else:
            self.get_logger().error(f"{mode.name} PX4 mode is not supported yet")
        return result

    def get_nav_state_name(self):
        """Gets nav state name from enum.

        Returns:
            string: name of state
        """
        mode = ""
        nav_state = self._mode.nav_state
        # TODO: remove pretty print once QGroundControl connection is established
        if nav_state == PositionalTelemetry.NAVIGATION_STATE_MANUAL:
            mode = "MANUAL"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_ALTCTL:
            mode = "ALTITUDE CONTROL"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_POSCTL:
            mode = "POSITION CONTROL"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_MISSION:
            mode = "MISSION"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_LOITER:
            mode = "LOITER"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_RTL:
            mode = "RETURN TO LAUNCH"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_LANDENGFAIL:
            mode = "LAND ON ENGINE FAIL"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_LANDGPSFAIL:
            mode = "LAND ON GPS FAIL"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_ACRO:
            mode = "ACRO"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_UNUSED:
            mode = "UNUSED"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_DESCEND:
            mode = "DESCEND"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_TERMINATION:
            mode = "TERMINATION"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_OFFBOARD:
            mode = "OFFBOARD"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_STAB:
            mode = "STABILIZED"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_RATTITUDE:
            mode = "RATTITUDE"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_TAKEOFF:
            mode = "TAKEOFF"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_LAND:
            mode = "LAND"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_FOLLOW_TARGET:
            mode = "FOLLOW TARGET"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_PRECLAND:
            mode = "PRECLAND"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_ORBIT:
            mode = "ORBIT"
        elif nav_state == PositionalTelemetry.NAVIGATION_STATE_MAX:
            mode = "MAX STATE"
        return mode

    def _set_offboard_mode(self, position:bool=False, velocity:bool=False, acceleration:bool=False, attitude:bool=False, body_rate:bool=False):
        """Set Offboard mode parameters.
        """  
        self._offboard_mode = OffboardControlMode()      
        self._offboard_mode.position = position
        self._offboard_mode.velocity = velocity
        self._offboard_mode.acceleration = acceleration
        self._offboard_mode.attitude = attitude
        self._offboard_mode.body_rate = body_rate

    def _publish_offboard_mode(self):
        """Publsih Offboard mode to ROS2.
        """        
        self._offboard_mode.timestamp = self._rtps_timestamp
        self._pub_offboard_mode.publish(self._offboard_mode)

    def _reset_offboard_changes(self, msg="Something has reset offboard..."):
        self.get_logger().warn(msg)
        self._changing_offboard = False
        self._setpoint_count = 0

    ################
    ## Action handlers
    ################
    def _handle_coordinate_goal(self, goal : GoToCoordinate):
        """Handler for a GoToCoordinate action. Runs in parallel to BasicVehicle
        """
        self.get_logger().info("GoToCoordinate: action started")
        coord = goal.request.coordinate
        success = self.set_coordinate(coord.position.x, coord.position.y, coord.position.z, coord.heading, frame=coord.frame)
        if not success:
            self.get_logger().error("GoToCoordinate: could not set coordinate")
            goal.abort()
            return GoToCoordinate.Result()

        # Loop until waypoint is reached
        feedback_msg = GoToCoordinate.Feedback()
        rate = self.create_rate(20)
        while True:
            rate.sleep()
            distance = self.distance_to_target()    
            reached = self.reached_target(distance=distance)     
            settled = self.settled()
            if distance is not None: #publish distance to target
                feedback_msg.distance = float(distance)
                goal.publish_feedback(feedback_msg)
            if goal.is_cancel_requested:
                self.get_logger().debug("GoToCoordinate: cancelling action")
                goal.canceled() #handle cancel action
                return GoToCoordinate.Result()
            if reached and settled:
                self.get_logger().info("GoToCoordinate: action finished!")
                goal.succeed() #handle sucess
                return GoToCoordinate.Result()

    def _handle_follow_coordinates_goal(self, goal : FollowCoordinates):
        """
            Handler for a GoToCoordintes action. Runs in parallel to BasicVehicle
        """
        self.get_logger().info("FollowCoordinates: action started")
        coords = goal.request.coordinates
        if np.isnan(goal.request.tolerance):
            tolerance = None
        else:
            tolerance = goal.request.tolerance
        rate = self.create_rate(20)
        feedback_msg = FollowCoordinates.Feedback()
        len_coords = len(coords)
        for i in range(len_coords):
            last = i==len_coords-1
            coord = coords[i]
            success = self.set_coordinate(coord.position.x, coord.position.y, coord.position.z, coord.heading, frame=coord.frame)
            if not success:
                self.get_logger().error("FollowCoordinates: could not set coordinate " + str(i + 1))
                goal.abort()
                return FollowCoordinates.Result()
            while True:
                rate.sleep()
                distance = self.distance_to_target()
                if last:
                    reached = self.reached_target(distance=distance)
                else:
                    reached = self.reached_target(distance=distance,tolerance=tolerance)
                if distance is not None:
                    feedback_msg.distance = float(distance)
                    goal.publish_feedback(feedback_msg)
                if goal.is_cancel_requested:
                    self.get_logger().debug("FollowCoordinates: cancelling action on waypoint " + str(i + 1))
                    goal.canceled()
                    return FollowCoordinates.Result()
                if last:
                    settled = self.settled()
                    if reached and settled:
                        self.get_logger().info("FollowCoordinates: finished waypoint " + str(i + 1))
                        break
                else:
                    if reached:
                        self.get_logger().info("FollowCoordinates: finished waypoint " + str(i + 1))
                        break
        goal.succeed()
        return FollowCoordinates.Result()

    def _handle_coordinate_cancel(self, cancel):
        """Handler for canceling a GoToCoordinate
        """
        self.halt()
        return CancelResponse.ACCEPT

    ################
    ## Service handlers
    ################
    def _handle_kill(self, req, res):
        """Method to kill the vehicle (stop motors immediately)
        """
        self.publish_info("Kill: sending")
        # self.disarm()
        res.success = self.send_vehicle_cmd(VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, 0.0, 21196) #Forces a dissarm
        return res

    # TODO: Implement way to get if state is armed or disarmed from rtps
    def _handle_state(self, req, res):
        """Handle vehicle state
        """
        if not self.is_armed():
            res.state = State.REST.value
        else:
            x, y, z = self._odometry.vx, self._odometry.vy, self._odometry.vz
            active_mode = self.in_offboard() or self._mode.nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_TAKEOFF or self._mode.nav_state == PositionalTelemetry.NAVIGATION_STATE_AUTO_LAND
            if active_mode and (abs(x) > 0.1 or abs(y) > 0.1 or abs(z) > 0.1):
                res.state = State.RUNNING.value
            elif active_mode:
                res.state = State.STANDBY.value
        res.status = 1
        return res    

    def _handle_arm(self, req, res):
        """Method to arm vehicle
        """
        self.publish_info("Arm: sending")
        res.success = self.arm() 
        return res

    def _handle_disarm(self, req, res):
        """Method to disarm vehicle
        """
        self.publish_info("Disarm: sending")
        res.success = self.disarm() 
        return res

    def _handle_set_offboard(self, req, res):
        """Method to set offboard mode
        """
        res.success = self.set_offboard()
        return res

    ################
    ## Subscriber callbacks
    ################
    def _callback_rtps_timestamp(self, msg):
        self._rtps_timestamp = msg.timestamp
    
    def _callback_home_coordinate(self, msg : HomePosition):
        if not (msg.valid_hpos and msg.valid_alt and msg.valid_lpos):
            self.get_logger().error("Home position is not valid. Trying again...", throttle_duration_sec=2.0)
            return
        self.get_logger().info(f"Home position set.", once=True)
        self._home_coordinate = msg

    def _callback_mode(self, msg : VehicleStatus): 
        self._mode = msg

    def _callback_odometry(self, msg):
        if np.isnan(msg.x) or np.isnan(msg.y) or np.isnan(msg.z):
            self.get_logger().warn("Odometry is NaN...", throttle_duration_sec=5.0)
            return
        self._odometry = msg

    def _callback_battery(self, msg:BatteryStatus):
        self._battery_status = msg

        battery = BatteryState()
        battery.header.stamp = self.get_clock().now().to_msg()
        battery.voltage = msg.voltage_v
        battery.current = msg.current_a
        battery.capacity = float(msg.capacity)
        battery.design_capacity = msg.design_capacity
        battery.percentage = msg.remaining
        battery.present = msg.connected
        battery.cell_voltage = [float(v) for v in msg.voltage_cell_v]
        self._pub_battery.publish(battery)

    def _callback_vehicle_land(self, msg):
        self._vehicle_landed = msg

    def _callback_velocity(self, msg : Twist):
        self.set_velocity(msg.linear.x, msg.linear.y, msg.linear.z, msg.angular.z)

    def _callback_mavlink(self, msg : Mavlink):
        def process(param):
            if param in ["", "nan", "NaN"]:
                param = np.nan
            else:
                param = float(param)
            return param
        # TODO: switch to using float instead of string when special floats are added https://github.com/ros/genmsg/issues/85
        param1 = process(msg.param1)
        param2 = process(msg.param2)
        param3 = process(msg.param3)
        param4 = process(msg.param4)
        param5 = process(msg.param5)
        param6 = process(msg.param6)
        param7 = process(msg.param7)
        self.send_vehicle_cmd(msg.command, param1, param2, param3, param4, param5, param6, param7, wait=False)

    def _callback_gps(self, msg):
        self._gps_position = msg

    def _callback_vehicle_command_ack(self, msg):
        self._vehicle_command_ack = msg

    def _callback_global_position(self, msg):
        self._global_position = msg

    def _callback_local_position(self, msg):
        # TODO: add check for msg.z_valid, this value is currently false on the field when drone is landed
        if not (msg.xy_valid and msg.xy_global and msg.z_global):
            self.get_logger().warn(f"XYZ position or global reference is not valid... xy: {msg.xy_valid}, xy_glob: {msg.xy_global}, z_glob: {msg.z_global}", throttle_duration_sec=5.0)
            self._local_position = None
            return
        self._local_position = msg

    def _callback_log_message(self, msg: LogMessage):
        severity = msg.severity
        log_level = Log.DEBUG
        text = bytes(msg.text[:msg.text.tolist().index(0)]).decode()

        if severity == PX4LogLevel.EMERGENCY:
            self.get_logger().fatal(f"PX4 EMERGENCY: {text}")
            log_level = Log.FATAL
        elif severity == PX4LogLevel.ALERT:
            self.get_logger().warn(f"PX4 ALERT: {text}")
            log_level = Log.WARN
        elif severity == PX4LogLevel.CRITICAL:
            self.get_logger().error(f"PX4 CRITICAL: {text}")
            log_level = Log.ERROR
        elif severity == PX4LogLevel.ERROR:
            self.get_logger().error(f"PX4 ERROR: {text}")
            log_level = Log.ERROR
        elif severity == PX4LogLevel.WARNING:
            self.get_logger().warn(f"PX4 WARN: {text}")
            log_level = Log.WARN
        elif severity == PX4LogLevel.NOTICE:
            self.get_logger().warn(f"PX4 NOTICE: {text}")
            log_level = Log.WARN
        elif severity == PX4LogLevel.INFO:
            self.get_logger().info(f"PX4 INFO: {text}")
            log_level = Log.INFO
        elif severity == PX4LogLevel.DEBUG:
            self.get_logger().debug(f"PX4 DEBUG: {text}")
            log_level = Log.DEBUG

        log = Log()
        log.stamp = self.get_clock().now().to_msg()
        log.level = int.from_bytes(log_level, "big")
        log.name = "PX4 Log"
        log.msg = text

        self._pub_px4_log.publish(log)

    ################
    ## Publisher methods
    ################
    def _publish_setpoint(self):
        self._trajectory_setpoint.timestamp = self._rtps_timestamp
        self._pub_trajectory_setpoint.publish(self._trajectory_setpoint)

    def _publish_telemetry(self):
        """Method Publishes vehicle's current pose to /namespace/telemetry
        """
        telem_dict = {
            "odom": self._odometry,
            "gps_position": self._gps_position,
            "local_position": self._local_position,
            "global_positiion": self._global_position,
        }
        telem_vars = np.array(list(telem_dict.values()))
        if None in telem_vars:
            mask = np.where(telem_vars == None)[0]
            names_ls = np.array(list(telem_dict.keys()))
            names = ", ".join(names_ls[mask])
            self.publish_error(f"{names} must be set before publishing telemetry", throttle_duration_sec=5.0)
            return

        ################
        ## Get Position from the vehicle
        ################

        vehicle_telem = PositionalTelemetry()
        vehicle_telem.header.stamp = self.get_clock().now().to_msg()
        # All local locations are relative to the home position (home = 0, 0, 0)
        vehicle_telem.ned.x = float(self.local_position.x)    # Meters north
        vehicle_telem.ned.y = float(self.local_position.y)     # Meters east
        vehicle_telem.ned.z = float(self.local_position.z)     # Meters down

        # All global positions are fully global (latitude, longitude, altitude msl)
        vehicle_telem.lla.x = float(self.global_position.lat)     # Latitude 
        vehicle_telem.lla.y = float(self.global_position.lon)     # Longitude
        vehicle_telem.lla.z = float(self.global_position.alt)     # Altitude msl

        # All velocities are in meters per second
        vehicle_telem.speed = float(self._gps_position.vel_m_s) # GPS ground speed, (metres/sec)
        
        # Battery voltage in volts
        vehicle_telem.battery = float(self._battery_status.voltage_v)

        # Quaternion orientation converted to roll, pitch, yaw 
        rpy = R.from_quat([self._odometry.q[1], self._odometry.q[2], self._odometry.q[3], self._odometry.q[0]]).as_euler('xyz')
        vehicle_telem.heading = float(rpy[2])        # Vehicle heading relative to north (0=North, 90= east)

        vehicle_telem.nav_state = self._mode.nav_state
        vehicle_telem.landed = self._vehicle_landed.landed
        vehicle_telem.armed = self.is_armed()
        
        vehicle_telem.satellites_used = self._gps_position.satellites_used	# Number of satellites used
        vehicle_telem.hdop = float(self._gps_position.hdop)			        # Horizontal dilution of precision
        vehicle_telem.eph = float(self._gps_position.eph)			        # GPS horizontal position accuracy (metres)
        vehicle_telem.fix_type = self._gps_position.fix_type                # 0-1: no fix, 2: 2D fix, 3: 3D fix, 4: RTCM code differential, 5: Real-Time Kinematic, float, 6: Real-Time Kinematic, fixed, 8: Extrapolated. Some applications will not use the value of this field unless it is at least two, so always correctly fill in the fix.

        nav_state_msg = String()
        mode = self.get_nav_state_name()
            
        nav_state_msg.data = f"{mode:.20s}"
        self._pub_nav_state.publish(nav_state_msg)
        self._pub_vehicle_telemetry.publish(vehicle_telem)

    def _publish_pose_odom_rpy(self):
        """Publish PoseStamped and OdometryStamped ROS2 msg and RPY msg.
        """       
        if self._odometry is None:
            return
         
        # Generic header message with timestamp so topics are synchronized
        header = Header()
        header.stamp = self.get_clock().now().to_msg()
        header.frame_id = f"{self.namespace}/home"      
        
        stamped_pose = PoseStamped()
        
        stamped_pose.header = header
        r = R.from_quat([self._odometry.q[1], self._odometry.q[2], self._odometry.q[3], self._odometry.q[0]])
        euler = r.as_euler('xyz')
        euler[2] = -euler[2]
        q = R.from_euler('xyz', euler).as_quat()
        stamped_pose.pose.orientation.x = float(q[0])
        stamped_pose.pose.orientation.y = float(q[1])
        stamped_pose.pose.orientation.z = float(q[2])
        stamped_pose.pose.orientation.w = float(q[3])

        stamped_pose.pose.position = get_point_msg_from_ned(self._odometry.x, self._odometry.y, self._odometry.z)

        odom = Odometry()
        
        odom.header = header
        odom.pose.pose = stamped_pose.pose      
        odom.twist.twist.linear.x = float(self._odometry.vx)
        odom.twist.twist.linear.y = float(self._odometry.vy)
        odom.twist.twist.linear.z = float(self._odometry.vz)
        odom.twist.twist.angular.x = float(self._odometry.rollspeed)
        odom.twist.twist.angular.y = float(self._odometry.pitchspeed)
        odom.twist.twist.angular.z = float(self._odometry.yawspeed)

        rpy = RPY()
        rpy.roll = float(euler[0])
        rpy.pitch = float(euler[1])
        rpy.yaw = float(euler[2])
        
        self._pub_pose.publish(stamped_pose)
        self._pub_odom.publish(odom)
        self._pub_rpy.publish(rpy)

    def _publish_target(self):
        """Method to publish the vehicle's current target coordinate as a PointStamped.
        """
        header = Header()
        header.stamp = self.get_clock().now().to_msg()
        header.frame_id = f"{self.namespace}/home"   

        current_target = PointStamped()
        current_target.header = header
        current_target.point = get_point_msg_from_ned(self._trajectory_setpoint.x, self._trajectory_setpoint.y, self._trajectory_setpoint.z)
        
        # Publish 
        self._pub_current_target.publish(current_target)

    def _publish_vehicle_position_navsatfix(self):
        """Method to publish the vehicle's LLA position, name and target in LLA.
        """
        if None in [self.global_position, self.local_position]:
            self.publish_warn("Waiting to send vehicle position and navsatfix...", throttle_duration_sec=5.0)
            return
        now = self.get_clock().now().to_msg()
        msg = VehiclePosition()
        msg.name = self.namespace
        
        global_position = self.global_position
        nav_msg = NavSatFix()
        header = Header()
        header.stamp = now
        nav_msg.header = header
        nav_msg.altitude = float(global_position.alt)
        nav_msg.latitude = float(global_position.lat)
        nav_msg.longitude = float(global_position.lon)
        msg.nav = nav_msg
        
        ned_target = (self._trajectory_setpoint.x, self._trajectory_setpoint.y, self._trajectory_setpoint.z)
        lla_target = navpy.ned2lla(ned_target, self.local_position.ref_lat, self.local_position.ref_lon, self.local_position.ref_alt)
        
        target = PointStamped()
        target.header.stamp = now
        target.header.frame_id = f"{self.namespace}/home" 
        target.point.x = lla_target[0]
        target.point.y = lla_target[1]
        target.point.z = lla_target[2]
        msg.target = target

        self._pub_vehicle_position.publish(msg)
        self._pub_navsatfix.publish(nav_msg)

    def _publish_hdop(self):
        """Method to publish HDOP."""
        if self._gps_position is None:
            self.get_logger().warn("GPS position must be set", throttle_duration_sec=5.0)
            return
        hdop_msg = Float32()
        hdop_msg.data = float(self._gps_position.hdop)			        # Horizontal dilution of precision
        self._pub_hdop.publish(hdop_msg)

    ########################
    ## Properties
    ########################
    @property
    def local_position(self):
        """The vehicles position in `LOCAL_NED` frame."""
        return self._local_position
    
    @property
    def global_position(self):
        """The vehicles position in `LLA` frame."""
        return self._global_position


def get_point_msg_from_ned(n, e, d):
    """Converts NED into ROS point message with proper frame.

    Returns:
        Point: ROS point message
    """
    point = Point()
    # tf and gazebo coordinate frame is forward (+x), left (+y), up (+z)
    point.x = float(n)
    point.y = float(-1.0*e)
    point.z = float(-1.0*d)
    return point


def main(args=None):
    rclpy.init(args=args)
    drone = BasicVehicle(namespace="iris_0")
    executor = MultiThreadedExecutor()
    rclpy.spin(drone, executor=executor)


if __name__ == "__main__":
    main()
