#!/usr/bin/env python
'''
Camera
======

ROS2 node for camera related functions such as taking pictures.
'''

import rclpy
import cv2
from companion_computing_interfaces.srv import TakePicture
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from rclpy.node import Node
import os
from vision_msgs.msg import Detection2DArray 
from rclpy.logging import get_logging_severity_from_string
from rclpy.callback_groups import ReentrantCallbackGroup

class Camera(Node):
    def __init__(self, log_level='info'):
        """Creates services and actions related to camera functionality.

        Args:
            log_level (str, optional): Log info for ROS. Defaults to 'info'.
        """
        super().__init__('camera_node')
        if log_level is not None:
            self.get_logger().set_level(get_logging_severity_from_string(log_level))
        self._default_callback_group = ReentrantCallbackGroup()  # ROS processes need to be run in parallel for this use case
        self.namespace = self.get_namespace().split("/")[-1]
        self._take_pic_service = self.create_service(TakePicture, "take_picture", self._handle_take_pic)
        self.bridge = CvBridge()
        self.darknet_active = False
        self._sub_image = self.create_subscription(
            Image,
            "realsense/color/image_raw",
            self._callback_image,
            1)
        self._sub_detections = self.create_subscription(
            Detection2DArray,
            "darknet/detections",
            self._callback_detections,
            10)

    def _callback_image(self, msg):
        """Stores most recent image"""
        self.picture = msg
        
    def _handle_take_pic(self, request, response):
        """Takes a picture"""
        filename = request.file_name
        self.get_logger().debug("Waiting for image topic!") 
        msg = self.picture
        try:
            self.get_logger().info("Saving image.")
            cv2_img = self.bridge.imgmsg_to_cv2(msg, "bgr8")
            path = os.environ['HOME'] + "/Documents/"
            cv2.imwrite(path + filename, cv2_img)
            self.get_logger().info("Saved picture to " + path + filename)
        except CvBridgeError as e:
            self.get_logger().error(e)
        return response


def main(args=None):
    rclpy.init(args=args)
    camera = Camera()
    rclpy.spin(camera)


if __name__ == "__main__":
    main()
