# companion-computing

Code related to NUAV's Companion Computing development projects.

## Website

Website for documentation can be found [here](https://aeronu.gitlab.io/nuav/companion-computing/).
