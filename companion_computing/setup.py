#!/usr/bin/env python

from setuptools import setup
from setuptools import find_packages
import os
from glob import glob

package_name = 'companion_computing'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        (os.path.join('share','ament_index','resource_index','packages'),
            [os.path.join('resource', package_name)]),
        (os.path.join('share', package_name), ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*'))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='bmchale',
    maintainer_email='mchale.blake@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'basic_vehicle = companion_computing.basic_vehicle:main',
            'basic_drone = companion_computing.basic_drone:main',
            'basic_rover = companion_computing.basic_rover:main',
            'basic_plane = companion_computing.basic_plane:main',
            'tf_broadcaster = companion_computing.utils.tf_broadcaster:main',
            'monitor_topics = companion_computing.utils.monitor_topics:main',
            'camera = companion_computing.subsystems.camera:main',
            'spawn_robot = companion_computing.utils.spawn_robot:main',
            "bag_plot = companion_computing.utils.bag_plot:main",
            "control_center = companion_computing.control_center:main",
            "shell = companion_computing.cli.shell:main",
            "gpio_listener = companion_computing.utils.gpio_node:main", 
            "i2c_controller = companion_computing.utils.i2c_node:main"
        ],
    },
)
