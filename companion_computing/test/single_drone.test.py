#!/usr/bin/env python3
'''
Run `ros2 test single_drone.test.py`
'''
from launch import LaunchDescription
from launch_ros.actions import Node
import launch_testing

import rclpy
from rclpy.executors import SingleThreadedExecutor


from companion_computing.cli.drone_client import DroneClient
from companion_computing.cli.common import complete_action_call
import pytest
import unittest


NAMESPACE = "drone_0"


class TestArmTakeoffLand(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # cls.context = Context()
        rclpy.init()
        cls.nh = DroneClient(namespace=NAMESPACE)
 
    @classmethod
    def tearDownClass(cls):
        rclpy.shutdown()

    def setUp(self):
        self.addCleanup(self.nh.destroy_subscription, self.nh._sub_pose)
        self.addCleanup(self.nh._cli_arm_takeoff.destroy)

    # https://answers.ros.org/question/322831/ros2-wait-for-action-using-rclpy/
    def test_arm_takeoff_land(self):
        executor = SingleThreadedExecutor()
        executor.add_node(self.nh)

        while self.nh._poses_received <= 10:
            self.nh.get_logger().info("Spinning til 10 poses are received.", throttle_duration_sec=2.0)
            executor.spin_once()

        # TODO: check that initial position is same after takeoff and land
        try:
            # Takeoff drone
            future = self.nh.send_arm_takeoff(altitude=3.0)
            self.assertTrue(complete_action_call(self.nh, executor, future, "arm_takeoff"))

            # Land drone 
            future = self.nh.send_land()
            self.assertTrue(complete_action_call(self.nh, executor, future, "land"))
        finally:
            executor.remove_node(self.nh)


if __name__ == '__main__':
    unittest.main(failfast=True)
