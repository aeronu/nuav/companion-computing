#!/usr/bin/env python3
'''
teleop_controller.launch.py

Starts specified number of vehicles in simulation with ardupilot connection and
basic_drone control system. If is_sim is set to false it will not start the
simulation.
'''


import os

from ament_index_python.packages import get_package_share_directory
from launch.launch_context import LaunchContext

import launch
import launch_ros.actions


def generate_launch_description():
    namespace = launch.substitutions.LaunchConfiguration('namespace')
    joy_dev = launch.substitutions.LaunchConfiguration('joy_dev')
    config_filepath = launch.substitutions.LaunchConfiguration('config_filepath')

    return launch.LaunchDescription([
        launch.actions.DeclareLaunchArgument('namespace', default_value='drone_0'),
        launch.actions.DeclareLaunchArgument('joy_dev', default_value='/dev/input/js0'),

        launch_ros.actions.Node(
            package='joy', executable='joy_node', name='joy_node',
            parameters=[{
                'dev': joy_dev,
                'deadzone': 0.3,
                'autorepeat_rate': 20.0,
            }]
        ),
        launch_ros.actions.Node(
            package='companion_computing', executable='velocity_joy_controller',
            output='screen'
        ),
    ])

