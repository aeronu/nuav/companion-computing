#!/usr/bin/env python3
'''
start_vehicles.launch.py

Starts specified number of vehicles in simulation with ardupilot connection and
basic_drone control system. If is_sim is set to false it will not start the
simulation.
'''
from launch import LaunchDescription
from launch.actions import OpaqueFunction
from companion_computing.launch.start_vehicles import launch_setup, LAUNCH_ARGS
from companion_computing.launch.common import get_launch_arguments


# https://github.com/colcon/colcon-core/issues/169
def generate_launch_description():   
    launch_description = []
    launch_description += get_launch_arguments(LAUNCH_ARGS)
    launch_description += [OpaqueFunction(function = launch_setup)] 
    return LaunchDescription(launch_description)
