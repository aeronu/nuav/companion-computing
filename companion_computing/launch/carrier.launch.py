#!/usr/bin/env python3
'''
carrier.launch.py

Starts only required parts for NUAV frogs on field.
'''
from launch import LaunchDescription
from launch.actions import OpaqueFunction
from launch import LaunchDescription
from companion_computing.launch.carrier import launch_setup, LAUNCH_ARGS
from companion_computing.launch.common import get_launch_arguments


def generate_launch_description():   
    launch_description = []
    launch_description += get_launch_arguments(LAUNCH_ARGS)
    launch_description += [OpaqueFunction(function = launch_setup)]
    return LaunchDescription(launch_description)
