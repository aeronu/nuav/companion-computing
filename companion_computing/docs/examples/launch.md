# Launch
To start any ROS2 application launch files should be used. There are three relevant launch files
for this package:
[`start_vehicles.launch.py`](companion_computing.launch.start_vehicles),
[`gazebo_world.launch.py`](companion_computing.launch.gazebo_world),
[`field.launch.py`](companion_computing.launch.field).  
## Run
To run an example launch file that starts two drones with names "leonardo" and "shredder" use:  
```bash
ros2 launch companion_computing start_vehicles.launch.py namespaces:="['leonardo','shredder']"
```  
Two drones can also be started by using the `nb` argument and setting it to `2` like this:  
```bash
ros2 launch companion_computing start_vehicles.launch.py nb:=2
```  
All launch arguments for each file can be found in their corresponding link above. Each argument
can be set by using `:=` similarily to the examples.
