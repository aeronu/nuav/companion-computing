# Shell Client
For quickly controlling the vehicle in simulation and on the field it is recommended to use the
drone shell client found [here](companion_computing.cli.shell).  
## Run
To use it run:
```bash
ros2 run companion_computing shell
```  
Once run you should see a shell like this:  
```bash
Welcome to drone shell! Type ? to list commands
> 
```  
This is a custom shell with commands to easily control the vehicle. To takeoff to 2 meters run:  
```bash  
> takeoff 2
```  
If using a namespace that is not 'drone_0', change the name with `set_name`. For example:  
```bash
> set_name drone_1
```
## Help
Other commands can be found by entering `?` or `help` into the console. If you ever want to know
what arguments a command takes pass `-h` as an argument. For example:  
```bash
> go_coord -h
Usage: go_coord [-h] x y z yaw {0, 1, 2}

Sends `go_to_coordinate` action.

positional arguments:
  x           x (m)
  y           y (m)
  z           z (m)
  yaw         yaw (rad)
  {0, 1, 2}   frame mapping {'LLA': 0, 'LOCAL_NED': 1, 'FRD': 2}

optional arguments:
  -h, --help  show this help message and exit
```  
The command `go_coord` takes 5 arguments with the first 4 being `x`, `y`, `z`, and `yaw`. The last
is the `frame` which is represented by an integer. For example, `2` corresponds to the `FRD` frame.  

Refer to the [`Frames`](../background/frames) documentation for more information.
## Exit
After running the vehicle exit the shell by either typing `exit` or pressing `CTRL+Z`.
