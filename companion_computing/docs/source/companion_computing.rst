

Companion Computing
===================

.. automodule:: companion_computing
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 2

   companion_computing.cli
   companion_computing.launch
   companion_computing.planning
   companion_computing.subsystems
   companion_computing.utils

Submodules
----------

.. toctree::
   :maxdepth: 2

   companion_computing.basic_drone
   companion_computing.basic_plane
   companion_computing.basic_rover
   companion_computing.basic_vehicle
   companion_computing.control_center
