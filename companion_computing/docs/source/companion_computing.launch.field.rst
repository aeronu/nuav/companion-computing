
Launch Arguments

=============== ==================================== ======================================================== 
name            default                              description                                              
=============== ==================================== ======================================================== 
base_name       drone                                Prefix for all vehicles.                                 
--------------- ------------------------------------ -------------------------------------------------------- 
log_level       debug                                Sets log level of ros nodes.                             
--------------- ------------------------------------ -------------------------------------------------------- 
serial_device   /dev/ttyTHS1                         Path to PX4 serial device port.                          
--------------- ------------------------------------ -------------------------------------------------------- 
cc_drone_config companion_computing/config/field.yml companion_computing drone config file for ROS parameters 
=============== ==================================== ======================================================== 

    
.. automodule:: companion_computing.launch.field
   :members:
   :undoc-members:
   :show-inheritance:
