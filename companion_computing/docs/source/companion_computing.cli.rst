

Cli
===

.. automodule:: companion_computing.cli
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 2

   companion_computing.cli.common
   companion_computing.cli.drone_client
   companion_computing.cli.shell
   companion_computing.cli.vehicle_client
