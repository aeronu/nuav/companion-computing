
Launch Arguments

======= =================== ===================================================== 
name    default             description                                           
======= =================== ===================================================== 
gui     true                Starts gazebo gui                                     
------- ------------------- ----------------------------------------------------- 
server  true                Starts gazebo server to run simulations in background 
------- ------------------- ----------------------------------------------------- 
verbose false               Starts gazebo server with verbose outputs             
------- ------------------- ----------------------------------------------------- 
world   franklin_park.world Gazebo world to load                                  
======= =================== ===================================================== 

    
.. automodule:: companion_computing.launch.gazebo_world
   :members:
   :undoc-members:
   :show-inheritance:
