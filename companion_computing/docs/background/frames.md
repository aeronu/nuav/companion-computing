# Frames  
When we refer to frames we are referring to the coordinate frames used to represent an object.
There can be many different frames that show the world from different perspectives.  
For instance, if a car is racing by a person, in the persons perspective the car is moving, while
in the car's perspective the person is moving. Using different views allows for us to easily
understand concepts that may be difficult.  
In this project we use 3 different frames `LLA`, `LOCAL_NED`, and `FRD`. The following sections
will describe each one in detail.  
## LLA
`LLA` is short for latitude, longitude, and altitude. It is also called the [Geographic coordinate
system](https://www.wikiwand.com/en/geographic_coordinate_system). `LLA` represents positions on a
spherical object, such as the Earth (kinda spherical). The measurements used are degrees for
latitude and longitude and meters for elevation. The degrees are relative to the Earth's equator
and prime meridian.  
![gcs](../images/frames_gcs.gif)  
`LLA` is one of the most common frames since Global Positioning Systems (GPS) use it.
In this project it is primarily used for getting the location of our vehicle. All other frames are
derived from this one through various conversions and estimations. `LLA` is not useful for
commanding actions in our case. It is difficult to represent small movement (relative to the earth)
using degrees. The next few frames will describe the alternative which use meters.  
For further information refer to IBM's ["Geographic coordinate systems"](https://www.ibm.com/docs/en/db2/11.5?topic=systems-geographic-coordinate) article.  
## LOCAL_NED  
`LOCAL_NED` refers to the representation of a vehicle's position using relative meters to its
initial location and true north. `NED` stands for north, east, and down. An example position
would be 5 meters north, 3 meters east, and 2 meters down.
![ned](../images/frames_ned.png)  
## FRD  
`FRD` is also known as the robot coordinate frame. `FRD` stands for forward, right, and down.
All positions are relative to the robot's current position and heading. This frame is the most
useful since it allows for us to command positions such as go right 5 meters.
