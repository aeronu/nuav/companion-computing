Welcome to companion-computing's documentation!
===========================================================

.. toctree::
   :caption: API Documentation
   :maxdepth: 1

    Companion Computing <source/companion_computing>
    Companion Computing Interfaces <https://aeronu.gitlab.io/nuav/companion-computing/api/companion_computing_interfaces/index-msg.html>

.. toctree::
    :caption: Examples
    :maxdepth: 1
    :glob:

    examples/*

.. toctree::
    :caption: Background
    :maxdepth: 1
    :glob:

    background/*
    PX4 ROS2 RTPS <https://dev.px4.io/v1.11_noredirect/en/middleware/micrortps.html#rtpsros2-interface-px4-fastrtps-bridge>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
