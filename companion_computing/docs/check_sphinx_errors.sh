#!/usr/bin/env bash

SPHINX_ERROR_FILE='sphinx-build-errors.txt'
GREP_CMD='grep -v "nonlocal image URI\| Duplicate ID:\|html_static_path" $SPHINX_ERROR_FILE'

if [ ! -e $SPHINX_ERROR_FILE ]; then
  echo "Sphinx build error file cannot be found: $SPHINX_ERROR_FILE";
  exit -1
fi

echo "Checking for Spinx build errors"
NUM_ERRORS=$(eval $GREP_CMD | wc -l);
echo "Number of Sphinx errors: $NUM_ERRORS";

if [ $NUM_ERRORS -ne 0 ]; then
  echo "Sphinx build errors:";
  eval $GREP_CMD
fi

exit $NUM_ERRORS
